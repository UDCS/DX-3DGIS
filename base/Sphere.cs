﻿using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace D3DMAP
{
   public class Sphere
    { 
        
       /// <param name="param">模型参数</param>
      /// <param name="radius">球体半径</param>
      /// <param name="slices">球体在水平方向的分块数目</param>
      /// <param name="stacks">球体在竖直方向的分块数目</param>
        public Sphere(D3dDraw D3device,Vector3 _m_center, float radius, short slices, short stacks)
               
        {
            this.m_radius = radius;
            this.m_slices = slices;
            this.m_stacks = stacks;
            m_center = _m_center;
            ComputeVertexs();//计算顶点
            ComputeIndices();//计算索引

            //构造mesh
            mesh = new Mesh(indices.Length / 3, vertices.Length, MeshFlags.Managed, CustomVertex.PositionColored.Format, D3device.Device);

            //顶点缓冲
            GraphicsStream vs = mesh.LockVertexBuffer(LockFlags.None);
            vs.Write(vertices);
            mesh.UnlockVertexBuffer();
            vs.Dispose();

            //索引缓冲
            GraphicsStream ids = mesh.LockIndexBuffer(LockFlags.None);
            ids.Write(indices);
            mesh.UnlockIndexBuffer();
            ids.Dispose();
        }
       public Mesh Getmesh()
        {
            return mesh;
        }
        private Vector3 m_center;//球体球心(模型坐标)
        private float m_radius;//球体半径
        private short m_slices;//球体在水平方向的分块数目
        private short m_stacks;//球体在竖直方向的分块数目
        private CustomVertex.PositionColored[] vertices;//定义球体网格顶点
        private short[] indices;//定义球体网格中三角形索引
        private Mesh mesh;//球体mesh网格
        /// <summary>
        /// 计算顶点
        /// </summary>
        /// <remarks>
        /// 球体上任意一点坐标可以通过球形坐标来表示(r半径，theta垂直角，alpha水平角)
        /// X=r*sin(theta)*cos(alpha);
        /// Y=r*cos(theta);
        /// Z=r*sin(theta)*sin(alpha);
        /// </remarks>
        private void ComputeVertexs()
        {
            vertices = new CustomVertex.PositionColored[(m_stacks + 1) * (m_slices + 1)];
            float theta = (float)Math.PI / m_stacks;
            float alpha = 2 * (float)Math.PI / m_slices;
            for (int i = 0; i < m_slices + 1; i++)
            {
                for (int j = 0; j < m_stacks + 1; j++)
                {
                    Vector3 pt = new Vector3();
                    pt.X = m_center.X + m_radius * (float)Math.Sin(i * theta) * (float)Math.Cos(j * alpha);
                    pt.Y = m_center.Y + m_radius * (float)Math.Cos(i * theta);
                    pt.Z = m_center.Z + m_radius * (float)Math.Sin(i * theta) * (float)Math.Sin(j * alpha);
                    vertices[j + i * (m_stacks + 1)].Position = pt;
                    vertices[j + i * (m_stacks + 1)].Color = Color.FromArgb(0, Color.Black).ToArgb();
                }
            }
        }

        /// <summary>
        /// 计算索引
        /// </summary>
        private void ComputeIndices()
        {
            indices = new short[6 * m_stacks * m_slices];
            for (short i = 0; i < m_slices; i++)
            {
                for (short j = 0; j < m_stacks; j++)
                {
                    indices[6 * (j + i * m_stacks)] = (short)(j + i * (m_stacks + 1));
                    indices[6 * (j + i * m_stacks) + 1] = (short)(j + i * (m_stacks + 1) + 1);
                    indices[6 * (j + i * m_stacks) + 2] = (short)(j + (i + 1) * (m_stacks + 1));
                    indices[6 * (j + i * m_stacks) + 3] = (short)(j + i * (m_stacks + 1) + 1);
                    indices[6 * (j + i * m_stacks) + 4] = (short)(j + (i + 1) * (m_stacks + 1) + 1);
                    indices[6 * (j + i * m_stacks) + 5] = (short)(j + (i + 1) * (m_stacks + 1));
                }
            }
        }

    }
}
