﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;

namespace D3DMAP
{
    public interface ID3DDraw
    {
        bool GetIsinit();
        void Initialize(D3dDraw D3device);
        void RenderDraw(D3dDraw D3device);
        Mesh GetMeshobj();
        object GetIDobj();
       
    }
    public class D3dDraw
    {
        Microsoft.DirectX.Direct3D.Device device;
        public Device Device
        {
            get
            {
                return device;
            }

            set
            {
                device = value;
            }
        }

        public camera Camera
        {
            get
            {
                return _Camera;
            }

            set
            {
                _Camera = value;
            }
        }

        camera _Camera;

    }
    public class intersection
    {
       public object ID;
        public float X;
        public float Y;
        public float Z;
    }
    public class camera
    {
        Microsoft.DirectX.Direct3D.Device device;
        public camera(Microsoft.DirectX.Direct3D.Device _device)
        {
            device = _device;
        }
        public Vector3 CamPostion = new Vector3(0, 30, 30);//定义摄像机位置
        public Vector3 CamTarget = new Vector3(0, 0, 0);//定义摄像机目标位置
        public Vector3 CamPostiondefault = new Vector3(0, 30, 30);//定义摄像机位置
        public Vector3 CamTargeydefault= new Vector3(0, 0, 0);
        public void SetUpCamera()//摄像机
        {
            Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new
    Vector3(0, 0, 1));
            device.Transform.View = viewMatrix;
        }
        int _Width; int _Height;
        public void setCamera(int Width,int Height)
        {
            _Width = Width;
            _Height = Height;
            Vector3 up = new Vector3(0, 0, 1);
           
            Matrix projection = Matrix.PerspectiveFovLH((float)Math.PI / 4,Width / Height,0.01f, 25.0f);
            device.Transform.Projection = projection;
            SetUpCamera();

        }
        public void Setdefault(Vector3 CamP, Vector3 CamT)
        {
            CamPostiondefault = CamP;
            CamTargeydefault = CamT;
        }
        public void Resetdefault()
        {
            CamPostion=CamPostiondefault;
            CamTarget= CamTargeydefault ;
        }
        public Viewport mViewPort;//视口大小
        public Matrix m_ProjectionMatrix; //上一次渲染采用的投影变换矩阵 Projection matrix used in last render.
        public Matrix m_ViewMatrix; //上一次渲染采用的观察矩阵 View matrix used in last render.
        public Matrix m_WorldMatrix = Matrix.Identity;//世界变换矩阵
        public void setCamera(float Z)
        {

           // Vector3 up = new Vector3(0, 1, 0);
           // Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, up);
            Matrix projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, _Width / _Height, 1.0f, Z);
            device.Transform.Projection = projection;
           // device.Transform.View = viewMatrix;

        }
        public void ComputeMatrix(Device m_Device3d, Matrix _m_WorldMatrix)
        {
            m_WorldMatrix = _m_WorldMatrix;
            m_ProjectionMatrix = m_Device3d.GetTransform(TransformType.Projection);
            m_ViewMatrix = m_Device3d.GetTransform(TransformType.View);
            mViewPort = m_Device3d.Viewport;
        }
        public void ComputeMatrix(Device m_Device3d)
        {
            m_WorldMatrix = m_Device3d.GetTransform(TransformType.World);
            m_ProjectionMatrix = m_Device3d.GetTransform(TransformType.Projection);
            m_ViewMatrix = m_Device3d.GetTransform(TransformType.View);
            mViewPort = m_Device3d.Viewport;
        }
        internal Vector3 UnProject(Device m_Device3d,Vector3 v1)
        {
            //m_WorldMatrix = m_Device3d.GetTransform(TransformType.World);
            //m_ProjectionMatrix = m_Device3d.GetTransform(TransformType.Projection);
            //m_ViewMatrix = m_Device3d.GetTransform(TransformType.View);
            //mViewPort = m_Device3d.Viewport;
            v1.Unproject(mViewPort, m_ProjectionMatrix, m_ViewMatrix, m_WorldMatrix);
            return v1;
        }
        internal Vector3 UnProject(Device m_Device3d, Vector3 v1, Matrix _m_WorldMatrix)
        {
            m_WorldMatrix = _m_WorldMatrix;
            //m_ProjectionMatrix = m_Device3d.GetTransform(TransformType.Projection);
            //m_ViewMatrix = m_Device3d.GetTransform(TransformType.View);
            //mViewPort = m_Device3d.Viewport;
            v1.Unproject(mViewPort, m_ProjectionMatrix, m_ViewMatrix, m_WorldMatrix);
            return v1;
        }
    }
}
