# DX 3DGIS 
DX-3DGIS-使用DX开发的基于高分辨卫星贴图的GIS产品。
主要功能

- 1.卫星贴图与地形文件解析展示合成三维地形展示。
- 2.广告牌的支持
- 3.模型显示支持
- 4.鼠标坐标点击支持
- 5.自主架构无第三方依赖，可简单便捷扩展
- 6.放大缩小，鼠标视角拖动功能

![输入图片说明](https://gitee.com/uploads/images/2018/0312/095416_b434ed79_598831.jpeg "QQ图片20180312094511.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0312/095440_b684de37_598831.png "QQ图片20180312094555.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0312/095450_9e35df7c_598831.png "QQ图片20180312094651.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0312/095726_cd95f789_598831.jpeg "QQ图片20180312094618.jpg")