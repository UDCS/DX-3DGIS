﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DX3DGIS
{
    class shp
    {
     // public  List<ArrayList> polygonsLIST = new List<System.Collections.ArrayList>();
        public ArrayList polygons = new ArrayList();
        public ArrayList polylines = new ArrayList();
        public ArrayList points = new ArrayList();
        int ShapeType; 
       public double xmin, ymin, xmax, ymax;
      public  struct PolyLine_shape
        {
            public double[] Box; //边界盒
            public int NumParts; //部分的数目
            public int NumPoints; //点的总数目
            public ArrayList Parts; //在部分中第一个点的索引
            public ArrayList Points; //所有部分的点
        }
      public  struct Polygon_shape
        {
            public double Box1; //边界盒
            public double Box2; //边界盒
            public double Box3; //边界盒
            public double Box4; //边界盒
            public int NumParts; //部分的数目
            public int NumPoints; //点的总数目
            public ArrayList Parts; //在部分中第一个点的索引
            public ArrayList Points; //所有部分的点
        }
     public   struct Point_shape
        {
            public double X;
            public double Y;
        }

       public void read(Stream filestr)
        {
            BinaryReader br = new BinaryReader(filestr);
            //读取文件过程

            br.ReadBytes(24);
            int FileLength = br.ReadInt32();//<0代表数据长度未知
            int FileBanben = br.ReadInt32();
            ShapeType = br.ReadInt32();
            xmin = br.ReadDouble();
            ymin =  br.ReadDouble();
            xmax = br.ReadDouble();
            ymax =  br.ReadDouble();
            double width = xmax - xmin;
            double height = ymax - ymin;
            //n1 = (float)(this.pictureBox1.Width * 0.9 / width);//x轴放大倍数
            //n2 = (float)(this.pictureBox1.Height * 0.9 / height);//y轴放大倍数
            br.ReadBytes(32);

            switch (ShapeType)
            {
                case 1:
                    points.Clear();
                    while (br.PeekChar() != -1)
                    {
                        Point_shape point = new Point_shape();

                        uint RecordNum = br.ReadUInt32();
                        int DataLength = br.ReadInt32();


                        //读取第i个记录
                        br.ReadInt32();
                        point.X = br.ReadDouble();
                        point.Y = -1 * br.ReadDouble();
                        points.Add(point);
                    }
                   
                    break;
                case 3:
                    polylines.Clear();
                    while (br.PeekChar() != -1)
                    {
                        PolyLine_shape polyline = new PolyLine_shape();
                        polyline.Box = new double[4];
                        polyline.Parts = new ArrayList();
                        polyline.Points = new ArrayList();

                        uint RecordNum = br.ReadUInt32();
                        int DataLength = br.ReadInt32();

                        //读取第i个记录
                        br.ReadInt32();
                        polyline.Box[0] = br.ReadDouble();
                        polyline.Box[1] = br.ReadDouble();
                        polyline.Box[2] = br.ReadDouble();
                        polyline.Box[3] = br.ReadDouble();
                        polyline.NumParts = br.ReadInt32();
                        polyline.NumPoints = br.ReadInt32();
                        for (int i = 0; i < polyline.NumParts; i++)
                        {
                            int parts = new int();
                            parts = br.ReadInt32();
                            polyline.Parts.Add(parts);
                        }
                        for (int j = 0; j < polyline.NumPoints; j++)
                        {

                            Point_shape pointtemp = new Point_shape();
                            pointtemp.X = br.ReadDouble();
                            pointtemp.Y = -1 * br.ReadDouble();
                            polyline.Points.Add(pointtemp);
                        }
                        polylines.Add(polyline);
                    }
                    
                    break;
                case 5:

                    polygons = new ArrayList();
                    while (br.PeekChar() != -1)
                    {
                        Polygon_shape polygon = new Polygon_shape();
                        polygon.Parts = new ArrayList();
                        polygon.Points = new ArrayList();

                        uint RecordNum = br.ReadUInt32();
                        int DataLength = br.ReadInt32();

                        //读取第i个记录
                        int m = br.ReadInt32();
                        polygon.Box1 = br.ReadDouble();
                        polygon.Box2 = br.ReadDouble();
                        polygon.Box3 = br.ReadDouble();
                        polygon.Box4 = br.ReadDouble();
                        polygon.NumParts = br.ReadInt32();
                        polygon.NumPoints = br.ReadInt32();
                        for (int j = 0; j < polygon.NumParts; j++)
                        {
                            int parts = new int();
                            parts = br.ReadInt32();
                            polygon.Parts.Add(parts);
                        }
                        for (int j = 0; j < polygon.NumPoints; j++)
                        {
                            Point_shape pointtemp = new Point_shape();
                            pointtemp.X = br.ReadDouble();
                            pointtemp.Y =  br.ReadDouble();
                            polygon.Points.Add(pointtemp);
                        }
                        polygons.Add(polygon);
                    }
                    //polygonsLIST.Add(polygons);
                    break;
            }
            br.Close();
        }
    
    }
}
