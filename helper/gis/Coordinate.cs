﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D3DMAP
{
  public  class Coordinate
    {
        double _minX, _maxY, _maxX, _minY;
        double _scaleX; double _scaleY;
        int _Width, _Height;
        public void SetCoordinate(int Width, int Height, double minX, double maxX, double minY, double maxY)
        {
            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;
            _Width = Width;
            _Height = Height;
            _scaleX = (_Width) / (_maxX - _minX);
            _scaleY = (_Height) / (_maxY - _minY);
            // this.PictureBox1.Refresh();
        }
        /// <summary>
        /// 坐标转像素
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="sX"></param>
        /// <param name="sY"></param>
        public void ToScreen(double pX, double pY, ref double sX, ref double sY)
        {
            sX = (double)((pX - _minX) * _scaleX);
            if (sX < 0)
                sX = 0;
            if (sX > _Width)
                sX = _Width-1;
            sY = (double)((_maxY - pY) * _scaleY);
            if (sY < 0)
                sY = 0;
            if (sY > _Height)
                sY = _Height-1;
        }
        public void ToScreen2(double pX, double pY, ref double sX, ref double sY)
        {
            sX = (double)((pX - _minX) * _scaleX);

            sY = (double)((_maxY - pY) * _scaleY);

        }
        /// <summary>
        /// 像素转坐标
        /// </summary>
        /// <param name="sX"></param>
        /// <param name="sY"></param>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        public void ToCoordinate(int sX, int sY, ref double pX, ref double pY)
        {
            pX = sX / _scaleX + _minX;
            pY = _maxY - sY / _scaleY;
        }
    }
}
