﻿using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using OSGeo.GDAL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace D3DMAP
{

   public class DEMode
    {
        public double[,] DEM;
        public double Maxlat;
        public double Maxlng;
        public double minlng;
        public double minlat;
        public int imgHeight;
        public int imgWidth ;
    }
  public  class DEMHepler
    {

        public static Matrix Scale(D3dDraw D3device,float size,float X,float Y,float Z)
        {
            Matrix mat_trans = Matrix.Identity, mat_world = Matrix.Identity;
            Matrix mat_transpose = D3device.Device.GetTransform(TransformType.View);
            Matrix World = D3device.Device.GetTransform(TransformType.World);

            Vector3 m_vEye = new Vector3(0,0,0); //观察者眼睛的位置
            Vector3 m_vLookat = new Vector3(X, Y, Z);
            
            Vector3 m_vUp = new Vector3(0, 0, 1);
            //Vector3 m_vView = Vector3.Normalize(m_vLookat - m_vEye);
            //Vector3 m_vCross = Vector3.Cross(m_vUp, m_vView);
            Matrix viewMatrix = Matrix.LookAtLH(m_vEye, m_vLookat, m_vUp);
            viewMatrix.Invert();
            //// 获取 billboard ( view matrix 的转置矩阵就是 billboard )   

            viewMatrix.M41 = 0.0f;
            viewMatrix.M42 = 0.0f;
            viewMatrix.M43 = 0.0f;

            Matrix newm = Matrix.Translation(m_vLookat);
           // Matrix viewMatrix = Matrix.Translation(m_vUp);
            Matrix matScal = Matrix.Identity;
           
            matScal.Scale(size, size, size);


            //* viewMatrix * newm
         //   World.RotateX(110);
            return matScal* viewMatrix * newm;
        }
      public  DEMode GetDEM(String heightMapPath)
        {
           // string heightMapPath = @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\\未命名(4)_高程.tif";//定义高度图路径 
            Gdal.AllRegister();
            OSGeo.GDAL.Gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES");//支持中文路径和名称
            string hdfFileName = heightMapPath;

            Dataset dsFile = Gdal.Open(hdfFileName, Access.GA_ReadOnly);
            String[] metadata = dsFile.GetMetadata("GEOLOCATION");


            double[] UpperLeft = GDALInfoGetPosition(dsFile, 0.0, 0.0);
            double[] LowerLeft = GDALInfoGetPosition(dsFile, 0.0, dsFile.RasterYSize);
            double[] UpperRight = GDALInfoGetPosition(dsFile, dsFile.RasterXSize, 0.0);
            double[] LowerRight = GDALInfoGetPosition(dsFile, dsFile.RasterXSize, dsFile.RasterYSize);
            //GDALInfoGetPosition(dsFile, dsFile.RasterXSize / 2, dsFile.RasterYSize / 2);中间值
            double[] max = new double[] { UpperRight[0], UpperLeft[1] };
            double[] min = new double[] { LowerLeft[0], LowerLeft[1] };
            Band band1 = dsFile.GetRasterBand(1);
            int imgWidth = dsFile.RasterXSize;
            int imgHeight = dsFile.RasterYSize;
            float ImgRatio = imgWidth / (float)imgHeight;  //影像宽高比
            double[] r = new double[imgWidth * imgHeight];
            band1.ReadRaster(0, 0, imgWidth, imgHeight, r, imgWidth, imgHeight, 0, 0);
            double[,] DEM = new double[imgWidth, imgHeight];
            double val = 0;int hasval = 0;
            band1.GetNoDataValue(out val,out hasval);
            // SetCoordinate(map.minlng, map.maxlng, map.minlat, map.maxlat);

            //像素转坐标c.ToCoordinate()
          //  Bitmap bit = new Bitmap(imgWidth, imgHeight);
            int count = 0;
            for (int I = 0; I < imgHeight; I++)
            {
                for (int j = 0; j < imgWidth; j++)
                {
                    DEM[j, I] = r[(I * imgWidth) + j]== val ? 0: (int)r[(I * imgWidth) + j];
                    //if (DEM[j, I] != 0)
                    //    bit.SetPixel(j, I, Color.White);
                }
            }
           // bit.Save("ttt.png");

            DEMode dm = new DEMode();
            dm.DEM = DEM;
            dm.minlng = min[0];
            dm.Maxlng = max[0];
            dm.minlat= min[1];
            dm.Maxlat = max[1];
            r = null;
            dsFile.Dispose();
            OSGeo.GDAL.Gdal.FinderClean();
            return dm;
        }
        public  DEMode GetDEMbypjw(String heightMapPath,int imgWidth,int imgHeight)
        {
            // string heightMapPath = @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\\未命名(4)_高程.tif";//定义高度图路径 
           



            DEMode dm = new DEMode();
            dm.imgHeight = imgHeight;
            dm.imgWidth = imgWidth;
            heightMapPath = heightMapPath.Substring(0, heightMapPath.LastIndexOf(".") + 1) + "pgw";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(heightMapPath))
            {
                double dw = Convert.ToDouble(sr.ReadLine()) * imgWidth;
                sr.ReadLine();
                sr.ReadLine();
                double dh = Convert.ToDouble(sr.ReadLine()) * imgHeight;
                dm.minlng = Convert.ToDouble(sr.ReadLine());
                dm.Maxlat = Convert.ToDouble(sr.ReadLine());
                dm.Maxlng = dm.minlng + dw;
                dm.minlat = dm.Maxlat + dh;

            }

           

            return dm;
        }
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        public byte[,] GetDEMbyimg(Bitmap bitm)
        {
            // string heightMapPath = @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\\未命名(4)_高程.tif";//定义高度图路径 
          
            int imgHeight = bitm.Height, imgWidth = bitm.Width;
          
            byte[,] DEM  = new byte[imgWidth, imgHeight];
            Rectangle rect = new Rectangle(0, 0, bitm.Width, bitm.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                 bitm.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, bitm.PixelFormat);
            byte temp = 0;
            try
            {
                unsafe
                {
                    //首地址
                    byte* ptr = (byte*)(bmpData.Scan0);
                    //灰度化
                    for (int i = 0; i < bmpData.Height; i++)
                    {
                        for (int j = 0; j < bmpData.Width; j++)
                        {

                            // temp = (byte)(0.299 * ptr[2] + 0.587 * ptr[1] + 0.114 * ptr[0]);
                            // ptr[0] = ptr[1] = ptr[2] = temp;
                            if (ptr[3] == 0)
                                DEM[j, i] = 0;
                            else
                                DEM[j, i] = 1;
                            ptr += 4;
                        }
                        //跳过空白块
                        ptr += bmpData.Stride - bmpData.Width * 4;
                    }
                }
                bitm.UnlockBits(bmpData);
                bmpData = null;
            }
            catch
            {
                bitm.UnlockBits(bmpData);
                //  像素转坐标c.ToCoordinate()

            }
            //Bitmap bit = new Bitmap(imgWidth, imgHeight);
            //for (int I = 0; I < imgHeight; I++)
            //{
            //    for (int j = 0; j < imgWidth; j++)
            //    {

            //        if (DEM[j, I] == 1)
            //            bit.SetPixel(j, I, Color.White);

            //    }
            //}
            //bit.Save("bbb.png");
            //解锁位图

            // SetCoordinate(map.minlng, map.maxlng, map.minlat, map.maxlat);



            // bitm.Dispose();

            // bitm = null;

            //GC.SuppressFinalize(bitm);
            GC.Collect();
            return DEM;
        }
        public  DEMode GetDEMbyBitmap(String heightMapPath)
        {
            // string heightMapPath = @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\\未命名(4)_高程.tif";//定义高度图路径 
            Bitmap bitm = new Bitmap(heightMapPath);
            DEMode dm = new DEMode();
            dm.imgHeight = bitm.Height;
            dm.imgWidth = bitm.Width;
            //int[,] DEM = new int[imgWidth, imgHeight];

            //// SetCoordinate(map.minlng, map.maxlng, map.minlat, map.maxlat);

            ////像素转坐标c.ToCoordinate()
            //for (int I = 0; I < imgHeight; I++)
            //{
            //    for (int j = 0; j < imgWidth; j++)
            //    {
            //       Color c= bitm.GetPixel(j, I);
            //        if (c.A == 0)
            //            DEM[j, I] = 0;
            //        else
            //           DEM[j, I] = 1;
            //    }
            //}
            //for (int I = 1; I < imgWidth-1; I++)
            //{
            //    for (int j = 1; j < imgHeight-1 ; j++)
            //    {
            //        Color c = bitm.GetPixel(I, j);
            //        int t = DEM[I - 1, j] + DEM[I + 1, j] + DEM[I, j - 1] + DEM[I, j + 1];
            //        if (t > 0 && t < 4 && DEM[I, j] == 1)
            //            DEM[I, j] = 2;


            //    }
            //}
             


          
            heightMapPath = heightMapPath.Substring(0, heightMapPath.LastIndexOf(".") + 1)+"pgw";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(heightMapPath))
            {
                double dw = Convert.ToDouble(sr.ReadLine())* dm.imgWidth;
                sr.ReadLine();
                sr.ReadLine();
                double dh= Convert.ToDouble(sr.ReadLine())* dm.imgHeight;
                dm.minlng =Convert.ToDouble( sr.ReadLine());
                dm.Maxlat = Convert.ToDouble(sr.ReadLine());
                dm.Maxlng = dm.minlng+ dw;
                dm.minlat = dm.Maxlat+ dh;

            }
           // bitm.Dispose();
            //dm.DEM = DEM;

            return dm;
        }

        public byte[,] edge(byte[,] image,int cx,int cy, Coordinate dencoorimng)
        {
            int row = image.GetLength(0); int col = image.GetLength(1);
           // var multiple = 24;
           //var xCount = (row - 1) / multiple;
           // var yCount = (col - 1) / multiple;
           // var cellWidth = row /xCount;
           // var cellHeight = col / yCount;

            //int M = image.GetLength(0);
            //int N = image.GetLength(1);
            byte[,] bianyuan = new byte[row, col];
            //double cpx = 0, cpy = 0;
            //dencoorimng.ToCoordinate(cx, cy, ref cpx, ref cpy);
            //List<double[]> bianyuanlist = new List<double[]>();
            int i, j;
            Bitmap bp = new Bitmap(row, col);
            for (i = 1; i < row - 1; i++)
                for (j = 1; j < col - 1; j++)
                {
                    
                    //int ii = (int)(i * cellWidth);
                    //int jj = (int)(j * cellHeight);
                    int ii = (int)(i );
                    int jj = (int)(j);
                    if (image[ii, jj] == 1)
                    {
                        int t = image[ii - 1, jj] + image[ii + 1, jj] + image[ii, jj - 1] + image[ii, jj + 1];
                        if (t > 0 && t < 4 )/*周围4个像素值介于1~3之间，*/
                        {
                            // bianyuan[i, j] = 1;             /*且当前像素为物体，则其必为边界*/
                            //double px = 0, py = 0;
                            //dencoorimng.ToCoordinate(ii, jj, ref px, ref py);
                            // bp.SetPixel(ii, jj, Color.White);
                            //double azimuth = calculate(cpx, px, cpy, py);
                            bianyuan[ii, jj] = 1;
                           // bianyuanlist.Add(new double[] { ii, jj, azimuth });
                        }
                    }
                }

            //        int len = bianyuanlist.Count;
            //for (i = 0; i < len; i++)
            //{
            //    for (j = i; j < len; j++)
            //    {
            //        if (bianyuanlist[i][2] > bianyuanlist[j][2])
            //        {
            //            double[] temp = new double[3];
            //            temp = bianyuanlist[j];
            //            bianyuanlist[j] = bianyuanlist[i];
            //            bianyuanlist[i] = temp;
            //        }
            //    }
            //}
            //double[] xy1 = bianyuanlist[0];
            //double[] xy2 = bianyuanlist[2];
            //var mastd = Math.Sqrt((1 -0) * (1 - 0) + (1 -0) * (1 -0));
            //  List<int> reindex = new List<int>();
          
            //bianyuanlist.CopyTo(arr);
            //try
            //{
            //    for (i = 0; i < len-1; i++)
            //    {
            //        if (bianyuanlist[i] != null)
            //        {
            //            xy1 = bianyuanlist[i];
            //            var min = 999.0;
            //            var mina = 90;
            //            int indexnext = i;
            //            for (j = 0; j < len; j++)
            //            {
            //                xy2 = arr[j];
            //                if (Math.Abs(xy1[2] - xy2[2]) < 90)
            //                {
            //                    var ang = Math.Abs(xy1[2] - xy2[2]);
            //                    var d = Math.Sqrt((xy1[0] - xy2[0]) * (xy1[0] - xy2[0]) + (xy1[1] - xy2[1]) * (xy1[1] - xy2[1]));
            //                    if (d <= min && (xy2[2] > xy1[2]) && d!=0)
            //                    {
            //                        min = d;
            //                        indexnext = j;
            //                    }
            //                }
            //                else
            //                    break;
            //            }
            //            if (indexnext != i + 1)
            //            {
            //                if (indexnext == i)
            //                {
            //                   // bianyuanlist[i + 1] = null;
                             
            //                }
            //                else
            //                { 
            //                    bianyuanlist[i + 1] = arr[indexnext];
            //                }
            //            }
            //        }

            //    }
            //}
            //catch 
            //{ }
          

            // Graphics g = Graphics.FromImage(bp);
            //PointF[] pf = new PointF[bianyuanlist.Count];
            //i = 0;
            //foreach (double[] bb in bianyuanlist)
            //{
            //    if(bb!=null)
            //    pf[i] = new PointF((int)bb[0], (int)bb[1]);
            //    else
            //        { }
            //    i++;
            //}
            //g.DrawLines(new Pen(Brushes.Red, 3), pf);
            //bp.Save("test.jpg");
            //g.Dispose();
            //bp.Dispose();
            return bianyuan;
        }

        public double angToRad(double angle_d)  ///度数转弧度
        {
            double Pi = 3.1415926535898;
            double rad1;
            rad1 = angle_d * Pi / 180;
            return rad1;
        }
        /// <summary>
        /// 计算两点北夹角
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lat2"></param>
        /// <param name="lng1"></param>
        /// <param name="lng2"></param>
        /// <returns></returns>
      public   double calculate(double lat1, double lat2, double lng1, double lng2)
        {
            double azimuth = 0;
            double averageLat = (lat1 + lat2) / 2;
            if (lat1 - lat2 == 0)
            { azimuth = 90; }
            else
            {
                azimuth = Math.Atan((lng1 - lng2) * Math.Cos(angToRad(averageLat)) / (lat1 - lat2)) * 180 / Math.PI;



            }

            if (lat1 > lat2)
            { azimuth = azimuth + 180; }
            if (azimuth < 0)
            { azimuth = 360 + azimuth; }
            return azimuth;
        }
        ///// <summary>
        ///// 计算两点北夹角
        ///// </summary>
        ///// <param name="lat1"></param>
        ///// <param name="lat2"></param>
        ///// <param name="lng1"></param>
        ///// <param name="lng2"></param>
        ///// <returns></returns>
        //double calculate(double x_point_s, double y_point_s, double x_point_e, double y_point_e)
        //{
        //    var angle = 0.0;
        //    var y_se = y_point_e - y_point_s;
        //    var x_se = x_point_e - x_point_s;
        //    if (x_se == 0 && y_se > 0)
        //        angle = 360;
        //    if (x_se == 0 && y_se < 0)
        //        angle = 180;
        //    if (y_se == 0 && x_se > 0)
        //        angle = 90;
        //    if (y_se == 0 && x_se < 0)
        //        angle = 270;
        //    if (x_se > 0 && y_se > 0)
        //        angle = Math.Atan(x_se / y_se) * 180 / Math.PI;
        //    else if (x_se < 0 && y_se > 0)

        //        angle = 360 + Math.Atan(x_se / y_se) * 180 / Math.PI;
        //    else if (x_se < 0 && y_se < 0)

        //        angle = 180 + Math.Atan(x_se / y_se) * 180 / Math.PI;
        //    else if (x_se > 0 && y_se < 0)

        //        angle = 180 + Math.Atan(x_se / y_se) * 180 / Math.PI;
        //    return angle;
        //}
        private  double[] GDALInfoGetPosition(Dataset ds, double x, double y)
        {
            double[] adfGeoTransform = new double[6];
            double dfGeoX, dfGeoY;
            ds.GetGeoTransform(adfGeoTransform);

            dfGeoX = adfGeoTransform[0] + adfGeoTransform[1] * x + adfGeoTransform[2] * y;
            dfGeoY = adfGeoTransform[3] + adfGeoTransform[4] * x + adfGeoTransform[5] * y;

            return new double[2] { dfGeoX, dfGeoY };
        }
    }
  
}
