﻿using D3DMAP.Projects;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX.DirectInput;
using Microsoft.DirectX.PrivateImplementationDetails;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace D3DMAP
{
   public class D3DHelper
    {
        Color backgroundcolor= Color.DarkSlateBlue;
        public Color Backgroundcolor
        {
            get
            {
                return backgroundcolor;
            }

            set
            {
                backgroundcolor = value;
            }
        }
        public void Resetdefault()
        {
            _camera.Resetdefault();
        }
        Form form = null;
        camera _camera;
        D3dDraw D3device;
        #region 初始化
        public D3DHelper(Form _form)
        {
            form = _form;
            form.MouseDown += OnMouseDown;
            form.MouseMove += OnMouseMove;
            form.MouseUp += OnMouseUp;
            form.MouseWheel += OnMouseWheel;
            form.Resize += Form_Resize;
            if (InitializeDirect3D())
            {
                D3device = new D3dDraw();
                D3device.Device = device;
                D3device.Camera = _camera;
                System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(Drawrun));
                t.Start();
                System.Threading.Thread t2 = new System.Threading.Thread(new System.Threading.ThreadStart(Drawinit));
                t2.Start();
                //  form.Paint += Form_Paint;

            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            Resize = true;
        }

        private void Form_Paint(object sender, PaintEventArgs e)
        {
            Render();
        }
        float i = 0;
         void Drawinit()
        {
            while (form.Created) //设置一个循环用于实时更新渲染状态
            {
                try
                {
                    for (int i = 0; i < layers.Count; i++)
                    {
                        if (layers[i] != null)
                            if (!layers[i].GetIsinit())
                                layers[i].Initialize(D3device);
                    }
                }
                catch(Exception e)
                {

                }
                System.Threading.Thread.Sleep(500);
               // Application.DoEvents(); //处理键盘鼠标等输入事件
            }
        }
          void Drawrun()
        {
            while (form.Created) //设置一个循环用于实时更新渲染状态
            {
               if(!stop)
                 Render();
                //保持device渲染，直到程序结束
               
                Application.DoEvents(); //处理键盘鼠标等输入事件
            }
        }
        
        public List<ID3DDraw> layers = new List<ID3DDraw>();

     


        Microsoft.DirectX.Direct3D.Device device;
        private Microsoft.DirectX.DirectInput.Device keyboard;//定义键盘设备
         bool InitializeDirect3D()
        {
            try
            {

                PresentParameters presentParams = new PresentParameters();
                presentParams.Windowed = true; //指定以Windows窗体形式显示
                presentParams.SwapEffect = SwapEffect.Discard; //当前屏幕绘制后  它将自动从内存中删除
                presentParams.AutoDepthStencilFormat =
DepthFormat.D16;
                presentParams.EnableAutoDepthStencil = true;
                presentParams.PresentationInterval =
               PresentInterval.Default;

                presentParams.BackBufferFormat = Format.Unknown;

                presentParams.AutoDepthStencilFormat = DepthFormat.D24S8;
                device = new Microsoft.DirectX.Direct3D.Device(0, Microsoft.DirectX.Direct3D.DeviceType.Hardware, form,
                   CreateFlags.SoftwareVertexProcessing, presentParams);
                _camera = new camera(device);
                var CamPostion = _camera.CamPostion;
                var CamTarget = _camera.CamTarget;
                _camera.setCamera(form.Width, form.Height);
                keyboard = new
Microsoft.DirectX.DirectInput.Device(SystemGuid.Keyboard);//实例化键盘对象
                keyboard.SetCooperativeLevel(form, CooperativeLevelFlags.Background
               | CooperativeLevelFlags.NonExclusive);
                keyboard.Acquire();//链接键盘设备
                InitialMouse();
               // LoadTexturesAndMaterials();

                //VertexDeclaration(new Vector3(0, 0, 0));


                return true;
            }
            catch (DirectXException e)
            {
                MessageBox.Show(e.ToString(), "Error"); //处理异常
                return false;
            }
        }

        private float angleY = 0.01f;//定义绕Y轴旋转变量
 
      
        private Microsoft.DirectX.DirectInput.Device mouseDevice;//定义鼠标设备


        public FillMode  DrawMode= FillMode.Solid;
        #endregion


        bool Resize = false;
        #region 绘制


        List<Vector3[]> listtemp = new List<Vector3[]>();

         void Render()
        {
            if (device == null) //如果device为空则不渲染
            {
                return;
            }
            if (Resize)
            {
                _camera.setCamera(form.Width, form.Height);
                Resize = false;
            }
            try
            {
                device.Clear(ClearFlags.Target | ClearFlags.ZBuffer,
            Backgroundcolor, 1.0f, 0);

                device.RenderState.CullMode = Cull.None;
                device.RenderState.FillMode = DrawMode;
                device.RenderState.Lighting = false;
                device.RenderState.Clipping = true;
                //Clockwise不显示按顺时针绘制的三角形

                // m_Device3d.RenderState.Ambient = World.Settings.StandardAmbientColor;

                device.RenderState.ZBufferEnable = true;
                device.RenderState.AlphaBlendEnable = true;
                //device.RenderState.SourceBlend = Blend.SourceAlpha;
                //device.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            }
            catch { }
            
            try
            {
            
                for (int i = 0; i < layers.Count; i++)
                {
                    if (layers[i] != null)
                        if (layers[i].GetIsinit())
                        {
                            layers[i].RenderDraw(D3device);
                            // _camera.SetUpCamera();
                            
                        }
                }
            //    //CreateFont
            //    device.BeginScene();
            //    try
            //    {
            //        foreach (Vector3[] v3 in listtemp)
            //        {
                    
            //                if (v3 != null)
            //                    DrawAxis2(device, v3);
                  
            //        }
            //    }
            //    catch { }
            //    device.EndScene();
            //}
            //catch (Exception e)
            //{
            //}
            //try
            //{
                
            //}
            //catch { }
            //try
            //{ 
            //     DrawAxis(device);

              
            }
            catch(Exception e)
            { }
            try
            {
                device.Present();
            }
            catch(Exception e)
            { }
            //  Draw2DString("测试！！", Color.Black,0,0,12);
           
            //KeyboardListen();

        }
        bool stop = false; 
        private Texture texture;//定义贴图变量
        private Material material;//定义材质变量
      
  
        //private void DrawAxis2(Microsoft.DirectX.Direct3D.Device device,Vector3[] v3)
        //{

        //    // device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
        //    //  device.TextureState[0].ColorOperation = TextureOperation.Disable;
          
              
        
        //    CustomVertex.PositionColored[] axisX2 = new CustomVertex.PositionColored[2];

        //    axisX2[1].X = v3[0].X;
        //    axisX2[1].Y = v3[0].Y;
        //    axisX2[1].Z = v3[0].Z;
        //    axisX2[1].Color = System.Drawing.Color.Green.ToArgb();
        //    //axisX2[0].X = D3device.Camera.CamPostion.X;
        //    //axisX2[0].Y = D3device.Camera.CamPostion.Y;
        //    //axisX2[0].Z = D3device.Camera.CamPostion.Z;
        //    //axisX2[0].X = 0;
        //    //axisX2[0].Y = 0;
        //    //axisX2[0].Z = 0;
        //    axisX2[0].X = v3[1].X;
        //    axisX2[0].Y = v3[1].Y;
        //    axisX2[0].Z = v3[1].Z;
        //    axisX2[0].Color = System.Drawing.Color.White.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisX2);


            
           
        //    //drawArgs.Device.Transform.World = drawArgs.WorldCamera.mWorldMatrix;

        //}
        //private void DrawAxis(Microsoft.DirectX.Direct3D.Device device)
        //{
        //    device.BeginScene();
        //    device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
        //  //  device.TextureState[0].ColorOperation = TextureOperation.Disable;
         
        //    CustomVertex.PositionColored[] axisX = new CustomVertex.PositionColored[2];

        //    axisX[0].X = 0;
        //    axisX[0].Y = 0;
        //    axisX[0].Z = 0;
        //    axisX[0].Color = System.Drawing.Color.Red.ToArgb();
        //    axisX[1].X = 1000;
        //    axisX[1].Y = 0;
        //    axisX[1].Z = 0;
        //    axisX[1].Color = System.Drawing.Color.Red.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisX);


        //    CustomVertex.PositionColored[] axisX2 = new CustomVertex.PositionColored[2];

        //    axisX2[0].X = _camera.CamTarget.X;
        //    axisX2[0].Y = _camera.CamTarget.Y;
        //    axisX2[0].Z = _camera.CamTarget.Z;
        //    axisX2[0].Color = System.Drawing.Color.Red.ToArgb();
        //    axisX2[1].X = _camera.CamTarget.X+1000;
        //    axisX2[1].Y = _camera.CamTarget.Y;
        //    axisX2[1].Z = _camera.CamTarget.Z;
        //    axisX2[1].Color = System.Drawing.Color.Red.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisX2);


        //    CustomVertex.PositionColored[] axisY = new CustomVertex.PositionColored[2];

        //    axisY[0].X = 0;
        //    axisY[0].Y = 0;
        //    axisY[0].Z = 0;
        //    axisY[0].Color = System.Drawing.Color.Green.ToArgb();
        //    axisY[1].X = 0;
        //    axisY[1].Y = 1000;
        //    axisY[1].Z = 0;
        //    axisY[1].Color = System.Drawing.Color.Green.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisY);

        //    CustomVertex.PositionColored[] axisY2 = new CustomVertex.PositionColored[2];

        //    axisY2[0].X = _camera.CamTarget.X;
        //    axisY2[0].Y = _camera.CamTarget.Y;
        //    axisY2[0].Z = _camera.CamTarget.Z;
        //    axisY2[0].Color = System.Drawing.Color.Green.ToArgb();
        //    axisY2[1].X = _camera.CamTarget.X;
        //    axisY2[1].Y = _camera.CamTarget.Y + 1000;
        //    axisY2[1].Z = _camera.CamTarget.Z;
        //    axisY2[1].Color = System.Drawing.Color.Green.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisY2);

        //    CustomVertex.PositionColored[] axisZ = new CustomVertex.PositionColored[2];

        //    axisZ[0].X = 0;
        //    axisZ[0].Y = 0;
        //    axisZ[0].Z = 0;
        //    axisZ[0].Color = System.Drawing.Color.Yellow.ToArgb();
        //    axisZ[1].X = 0;
        //    axisZ[1].Y = 0;
        //    axisZ[1].Z = 1000;
        //    axisZ[1].Color = System.Drawing.Color.Yellow.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisZ);

        //    CustomVertex.PositionColored[] axisZ2 = new CustomVertex.PositionColored[2];

        //    axisZ2[0].X = _camera.CamTarget.X;
        //    axisZ2[0].Y = _camera.CamTarget.Y;
        //    axisZ2[0].Z = _camera.CamTarget.Z;
        //    axisZ2[0].Color = System.Drawing.Color.Yellow.ToArgb();
        //    axisZ2[1].X = _camera.CamTarget.X;
        //    axisZ2[1].Y = _camera.CamTarget.Y;
        //    axisZ2[1].Z = _camera.CamTarget.Z + 1000;
        //    axisZ2[1].Color = System.Drawing.Color.Yellow.ToArgb();
        //    device.DrawUserPrimitives(PrimitiveType.LineStrip, 1, axisZ2);

        //    device.EndScene();
        //    //drawArgs.Device.Transform.World = drawArgs.WorldCamera.mWorldMatrix;

        //}
        #endregion

        public intersection PerformSelectionAction(int X, int Y )
        {
            stop = true;
            System.Threading.Thread.Sleep(100);
            Vector3 v1 = new Vector3();
            v1.X = X;
            v1.Y = Y;
            v1.Z = 0;
            Vector3 v2 = new Vector3();
            v2.X = X;
            v2.Y = Y;
            v2.Z = 1;
            D3device.Camera.ComputeMatrix(device);
           Vector3 rayPos = D3device.Camera.UnProject(device, v1);
            Vector3 rayDir = D3device.Camera.UnProject(device, v2) - rayPos;
 
          
            //判断模型是否与射线相交
            bool result = false;
            
            List<IntersectInformation> listindex = new List<IntersectInformation>();
            Vector3 appPoint = new Vector3();
           List <object> listobj = new List<object>();
            intersection iter =null;
            for (int i = 0; i < layers.Count; i++)
            {
                if (layers[i] != null)
                    if (layers[i].GetIsinit())
                    {
                      
                        if (layers[i] is LandSurface)
                        {
                            //D3device.Camera.ComputeMatrix(device);
                            //将屏幕坐标装换为世界坐标，构造一个射线
                           
                            IntersectInformation index = new IntersectInformation();
                            bool result2 = this.IntersectWithRay(layers[i].GetMeshobj(), rayPos, rayDir, ref index);

                            if (result2)
                            {
                                iter = new intersection();
                                try
                                {
                                    //short gg = (layers[i] as LandSurface).terrainlevelzreo.indices[index.FaceIndex * 3];
                                    //CustomVertex.PositionColoredTextured t1 = (layers[i] as LandSurface).terrainlevelzreo.vertices[gg];
                                    //CustomVertex.PositionColoredTextured t2 = (layers[i] as LandSurface).terrainlevelzreo.vertices[gg + 1];
                                    //CustomVertex.PositionColoredTextured t3 = (layers[i] as LandSurface).terrainlevelzreo.vertices[gg + 2];
                                    appPoint = rayPos + (rayDir * (float)index.Dist);
                                }
                                catch { }
                              

                                iter.X = appPoint.X;
                                iter.Y = appPoint.Y;
                                iter.Z = appPoint.Z;
                            }
                            //return layers[i].GetIDobj();
                            //CustomVertex.PositionColoredTextured vertices=  (layers[i] as LandSurface).terrainlevelzreo.vertices[index.FaceIndex];
                        }
                        else
                        {
                            //Matrix old= device.Transform.World;
                            //D3device.Camera.ComputeMatrix(device, world);
                            ////将屏幕坐标装换为世界坐标，构造一个射线
                            //device.SetTransform(TransformType.World, world);
                            //  device.Present();
                            Mesh ms = layers[i].GetMeshobj();
                            IntersectInformation index = new IntersectInformation();
                            if (ms != null)
                            {
                                bool result2 = this.IntersectWithRay(ms, rayPos, rayDir, ref index);
                                if (result2)
                                {
                                    listindex.Add(index);
                                    listobj.Add(layers[i].GetIDobj());
                                    result = true;
                                }
                            }
                          //  device.SetTransform(TransformType.World, old);
                            //device.Present();
                        }
                    }
            }
            if (result)
            {
                float min = 999999; int temindex = -1;
                for (int i = 0; i < listindex.Count; i++)
                {


                    if (listindex[i].Dist < min)
                    {
                        min = listindex[i].Dist;
                        temindex = i;
                    }

                }

                //iter.X = appPoint.X;
                //iter.Y = appPoint.Y;
                //iter.Z = appPoint.Z;
                listindex.Clear();
                listindex = null;
                if (temindex != -1)
                    iter.ID = listobj[temindex];
            }
            //if (result)
            // drawArgs.Selection.Add(this);
            stop = false;
            return iter;
           
        }


      
     

        /// <summary>
        /// 判断模型是否与射线相交
        /// </summary>
        /// <param name="rayPos">射线原点</param>
        /// <param name="rayDir">射线方向</param>
        /// <param name="drawArgs">绘制参数</param>
        /// <returns>如果相交返回True,否则返回False</returns>
         bool IntersectWithRay(Mesh mesh, Vector3 rayPos, Vector3 rayDir,ref IntersectInformation index)
        {
           
            bool isSelected = false;
            try
            {
                //选中距离
                float dis;
                IntersectInformation insertInfo;

                //构造一条基于模型本体坐标系的射线，用于判断射线是否与模型相交
                Matrix invert = Matrix.Invert(_camera.m_WorldMatrix);
                Vector3 rayPos1 = Vector3.TransformCoordinate(rayPos, invert);
                Vector3 rayPos2 = rayPos + rayDir;
                rayPos2 = Vector3.TransformCoordinate(rayPos2, invert);
                Vector3 rayDir1 = rayPos2 - rayPos1;
                //Ray ray1 = new Ray(rayPos1, rayDir1);
                //IntersectInformation[] all = new IntersectInformation[50];
                //mesh.IntersectSubset(0, rayPos1, rayDir1, out all);

                 isSelected = mesh.Intersect(rayPos1, rayDir1, out insertInfo);

                Vector3 appPoint = rayPos1 + (rayDir1 * (float)index.Dist);

                // iNumberArea

                //  this.m_selectedMinDistance = insertInfo.Dist;
                index = insertInfo;
            }
            catch (Exception caught)
            {
               // Utility.Log.Write(caught);
            }
            return isSelected;
        }


        #region 键盘操作
        private void KeyboardListen()
        {
            KeyboardState keys = keyboard.GetCurrentKeyboardState();
            var CamTarget = _camera.CamTarget;
            var CamPostion = _camera.CamPostion;
            if (keys[Key.Escape])
            {
                DialogResult result = MessageBox.Show("是否退出程序？", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
            Vector4 tempV4;
            Matrix currentView = device.Transform.View;//当前摄像机的视图矩阵
            if (keys[Key.Left])
            {
                _camera.CamPostion.Subtract(CamTarget);
                tempV4 = Vector3.Transform(CamPostion,
               Matrix.RotationQuaternion(Quaternion.RotationAxis(new Vector3(currentView.M12,
               currentView.M22, currentView.M32), -angleY)));
                 CamPostion.X = tempV4.X + CamTarget.X; CamPostion.Y = tempV4.Y + CamTarget.Y;
                CamPostion.Z = tempV4.Z + CamTarget.Z;
                Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new Vector3(0, 1, 0));
                device.Transform.View = viewMatrix;
            }
            if (keys[Key.Right])
            {
               
                CamPostion.Subtract(CamTarget);
                tempV4 = Vector3.Transform(CamPostion,
               Matrix.RotationQuaternion(
                Quaternion.RotationAxis(new Vector3(currentView.M12,
               currentView.M22, currentView.M32), angleY)));
                CamPostion.X = tempV4.X + CamTarget.X;
                CamPostion.Y = tempV4.Y + CamTarget.Y;
                CamPostion.Z = tempV4.Z + CamTarget.Z;
                Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new Vector3(0, 1, 0));
                device.Transform.View = viewMatrix;
            }
            if (keys[Key.Up])
            {
               
                CamPostion.Subtract(CamTarget);
                tempV4 = Vector3.Transform(CamPostion,
               Matrix.RotationQuaternion(
                Quaternion.RotationAxis(new
               Vector3(device.Transform.View.M11
                , device.Transform.View.M21, device.Transform.View.M31),
               -angleY)));
                CamPostion.X = tempV4.X + CamTarget.X;
                CamPostion.Y = tempV4.Y + CamTarget.Y;
                CamPostion.Z = tempV4.Z + CamTarget.Z;
                Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new Vector3(0, 1, 0));
                device.Transform.View = viewMatrix;
            }
            if (keys[Key.Down])
            {
                
                CamPostion.Subtract(CamTarget);
                tempV4 = Vector3.Transform(CamPostion,
               Matrix.RotationQuaternion(
                Quaternion.RotationAxis(new
               Vector3(device.Transform.View.M11, device.Transform.View.M21, device.Transform.View.M31),
               angleY)));
                CamPostion.X = tempV4.X + CamTarget.X;
                CamPostion.Y = tempV4.Y + CamTarget.Y;
                CamPostion.Z = tempV4.Z + CamTarget.Z;
                Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new Vector3(0, 1, 0));
                device.Transform.View = viewMatrix;
            }
               _camera.CamTarget= CamTarget;
               _camera.CamPostion= CamPostion;

        }
        #endregion


        #region 鼠标操作
         void InitialMouse()
        {
            mouseDevice = new
                Microsoft.DirectX.DirectInput.Device(SystemGuid.Mouse);//实例化鼠标对象
            mouseDevice.Properties.AxisModeAbsolute = true;
            mouseDevice.SetCooperativeLevel(form,
           CooperativeLevelFlags.NonExclusive | CooperativeLevelFlags.Background);
            mouseDevice.Acquire();
        }


        protected  void OnMouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                mouseLastX = e.X;
                mouseLastY = e.Y;
                isRotateByMouse = true;
            }
            else if (e.Button == MouseButtons.Right)
            {
                mouseLastX = e.X;
                mouseLastY = e.Y;
                isMoveByMouse = true;
            }
        }
        private int mouseLastX, mouseLastY;//记录鼠标按下时的坐标位置
        private bool isRotateByMouse = false;//记录是否由鼠标控制旋转
        private bool isMoveByMouse = false;//记录是否由鼠标控制移动

     
        protected  void OnMouseMove(object sender, MouseEventArgs e)
        {
            var CamTarget = _camera.CamTarget;
            var CamPostion = _camera.CamPostion;
            if (isRotateByMouse)
            {
               
                Matrix currentView = device.Transform.View;//当前摄像机的视图矩 阵
                float tempAngleY = 2 * (float)(e.X - mouseLastX) / form.Width;
                CamPostion.Subtract(CamTarget);

                Vector4 tempV4 = Vector3.Transform(CamPostion, Matrix.RotationQuaternion(
 Quaternion.RotationAxis(new Vector3(currentView.M12,
currentView.M22, currentView.M32), tempAngleY)));
                CamPostion.X = tempV4.X;
                CamPostion.Y = tempV4.Y;
                CamPostion.Z = tempV4.Z;
                float tempAngleX = 4 * (float)(e.Y - mouseLastY) / form.Height;
                tempV4 = Vector3.Transform(CamPostion,
               Matrix.RotationQuaternion(
                Quaternion.RotationAxis(new Vector3(currentView.M11,
               currentView.M21, currentView.M31), tempAngleX)));
                CamPostion.X = tempV4.X + CamTarget.X;
                CamPostion.Y = tempV4.Y + CamTarget.Y;
                CamPostion.Z = tempV4.Z + CamTarget.Z;
               // Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new
               //Vector3(0, 1, 0));
               // device.Transform.View = viewMatrix;

                mouseLastX = e.X;
                mouseLastY = e.Y;
            }
            else if (isMoveByMouse)
            {
                Matrix currentView = device.Transform.View;//当前摄像机的视图矩阵
                float moveFactor = 0.001f;
                CamTarget.X += -moveFactor * ((e.X - mouseLastX) * currentView.M11
               - (e.Y - mouseLastY) * currentView.M12);
                CamTarget.Y += -moveFactor * ((e.X - mouseLastX) * currentView.M21
               - (e.Y - mouseLastY) * currentView.M22);
                CamTarget.Z += -moveFactor * ((e.X - mouseLastX) * currentView.M31
               - (e.Y - mouseLastY) * currentView.M32);
                CamPostion.X += -moveFactor * ((e.X - mouseLastX) * currentView.M11
               - (e.Y - mouseLastY) * currentView.M12);
                CamPostion.Y += -moveFactor * ((e.X - mouseLastX) * currentView.M21
               - (e.Y - mouseLastY) * currentView.M22);
                CamPostion.Z += -moveFactor * ((e.X - mouseLastX) * currentView.M31
 - (e.Y - mouseLastY) * currentView.M32);

               // Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new
               //Vector3(0, 1, 0));
               // device.Transform.View = viewMatrix;
                mouseLastX = e.X;
                mouseLastY = e.Y;
            }
            _camera.CamTarget = CamTarget;
            _camera.CamPostion = CamPostion;
            _camera.SetUpCamera();
        }
        protected  void OnMouseUp(object sender, MouseEventArgs e)
        {
            isRotateByMouse = false;
            isMoveByMouse = false;
        }
        protected  void OnMouseWheel(object sender, MouseEventArgs e)
        {
            var CamTarget = _camera.CamTarget;
            var CamPostion = _camera.CamPostion;
            float scaleFactor = -(float)e.Delta / 2000 + 1f;
            CamPostion.Subtract(CamTarget);
            CamPostion.Scale(scaleFactor);
            CamPostion.Add(CamTarget);
 //           Matrix viewMatrix = Matrix.LookAtLH(CamPostion, CamTarget, new
 //Vector3(0, 1, 0));
 //           device.Transform.View = viewMatrix;
            _camera.CamTarget = CamTarget;
            _camera.CamPostion = CamPostion;
            _camera.SetUpCamera();
        }

        #endregion
    }
}
