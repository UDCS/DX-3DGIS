﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;

namespace D3DMAP.Projects
{
    public class Xmode : ID3DDraw
    {
        float X, Y, Z, size;
        String path;
        public Xmode(float _X, float _Y, float _Z, float _size,String _path)
        {
            X = _X;Y = _Y;Z = _Z; size = _size; path = _path;
        }
        public object ID;
        public object GetIDobj()
        {
            return ID;
        }
        Mesh mesh = null;
        Material meshMaterials;
        Texture[] meshTextures;
        Texture DmeshTextures;
        Microsoft.DirectX.Direct3D.Material[] meshMaterials1;
        public bool GetIsinit()
        {
           

            return init;

        }
        bool init = false;
        public Mesh GetMeshobj()
        {
            return mesh;
        }

        public void Initialize(D3dDraw D3device)
        {
            meshMaterials = new Material();
            meshMaterials.Ambient = System.Drawing.Color.White;		//材质如何反射环境光
            meshMaterials.Diffuse = System.Drawing.Color.White;// Color.FromArgb(127, 255, 255, 255);//材质如何反射灯光

            D3device.Device.Material = meshMaterials;//指定设备的材质
            DmeshTextures= TextureLoader.FromFile(D3device.Device, "1.1.bmp");
            ExtendedMaterial[] materials = null;
            //下句从tiger.x文件中读入3D图形(立体老虎)
            mesh = Mesh.FromFile(@path, MeshFlags.SystemMemory, D3device.Device, out materials);
            if (meshTextures == null)//如果还未设置纹理，为3D图形增加纹理和材质
            {
                meshTextures = new Texture[materials.Length];//纹理数组
                meshMaterials1 = new Microsoft.DirectX.Direct3D.Material[materials.Length];//材质数组
                for (int i = 0; i < materials.Length; i++)//读入纹理和材质
                {
                    meshMaterials1[i] = materials[i].Material3D;
                    meshMaterials1[i].Ambient = meshMaterials1[i].Diffuse;
                    if(materials[i].TextureFilename!=null)
                    meshTextures[i] = TextureLoader.FromFile(D3device.Device, @materials[i].TextureFilename);
                    
                }
            }
            init = true;
        }

        public void RenderDraw(D3dDraw D3device)
        {
            D3device.Device.BeginScene();


            Matrix World = D3device.Device.GetTransform(TransformType.World);
            D3device.Device.Transform.World = DEMHepler.Scale(D3device, size, X, Y, Z);
          
            VertexFormats format = D3device.Device.VertexFormat;
            FillMode currentCull = D3device.Device.RenderState.FillMode;
            int currentColorOp = D3device.Device.GetTextureStageStateInt32(0, TextureStageStates.ColorOperation);
            int zBuffer = D3device.Device.GetRenderStateInt32(RenderStates.ZEnable);
            try
            {

                D3device.Device.RenderState.ZBufferEnable = true;		 	//允许使用深度缓冲
                D3device.Device.RenderState.Ambient = System.Drawing.Color.White;//设定环境光为白色
                D3device.Device.Lights[0].Type = LightType.Directional;  	//设置灯光类型
                D3device.Device.Lights[0].Diffuse = Color.White;			//设置灯光颜色
                D3device.Device.Lights[0].Direction = new Vector3(0, 0, 1);	//设置灯光位置
                D3device.Device.Lights[0].Update();						//更新灯光设置，创建第一盏灯光
                D3device.Device.Lights[0].Enabled = true;					//使设置有效

                D3device.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                D3device.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                D3device.Device.TextureState[0].ColorArgument2 = TextureArgument.Diffuse;
                D3device.Device.TextureState[0].AlphaOperation = TextureOperation.Disable;
                // drawArgs.Device.TextureState[0].AlphaArgument1 = TextureArgument.TextureColor;

                for (int i = 0; i < meshMaterials1.Length; i++)//Mesh中可能有多个3D图形，逐一显示
                {
                    D3device.Device.Material = meshMaterials1[i];//设定3D图形的材质
                    if(meshTextures[i]!=null)
                    D3device.Device.SetTexture(0, meshTextures[i]);//设定3D图形的纹理
                    else
                        D3device.Device.SetTexture(0, DmeshTextures);
                   mesh.DrawSubset(i);//显示该3D图形
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

                D3device.Device.SetTransform(TransformType.World, World);
                D3device.Device.VertexFormat = format;
                D3device.Device.RenderState.FillMode = currentCull;
                D3device.Device.SetTextureStageState(0, TextureStageStates.ColorOperation, currentColorOp);
                D3device.Device.SetRenderState(RenderStates.ZEnable, zBuffer);
            }
            D3device.Device.EndScene();

        }
    }
}
