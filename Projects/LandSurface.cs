﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;
using System.IO;
using static DX3DGIS.shp;
using DX3DGIS;

namespace D3DMAP.Projects
{
    public class LandSurface : ID3DDraw
    {
        string fileimg; String fildem;float hyperbole;
        public float basehigh { get; set; }
        public float transparency { get; set; }
        public bool Surrounded { get; set; }
        public String shpstr = "";
        public Pen shpPens= new Pen(Color.Red,10);
        public int multiple = 24;
        /// <summary>
        /// 载入地形与地表
        /// </summary>
        /// <param name="fileimg">地表图</param>
        /// <param name="fildem">高程图</param>
        public LandSurface(string _fileimg, String _fildem, float _hyperbole)
        {
            fileimg = _fileimg;
            fildem = _fildem;
            hyperbole = _hyperbole;
            basehigh = 0;
            transparency = 1;
            Surrounded = true;
            
        }
        public object ID { get; set; }
        bool isinit = false;
        private Material material;//定义材质变量
       public terrain terrainlevelzreo = new terrain();
        private void LoadTexturesAndMaterials(Microsoft.DirectX.Direct3D.Device device)//导入贴图和材质
        {
            material = new Material();
            material.Diffuse = Color.Transparent;
            material.Specular = Color.FromArgb(83, 83, 83);
            material.SpecularSharpness = 15.0F;
          

        }
        Bitmap Drawshp(Bitmap bitm, Coordinate imgcd) 
        {
            shp sh = new shp();
            FileStream fs = System.IO.File.Open(shpstr, FileMode.Open);
            sh.read(fs);
            fs.Close();
            Graphics g = Graphics.FromImage(bitm);
            if (sh.polylines.Count < 1)
                return bitm;
            
            for (int i = 0; i < sh.polylines.Count; i++)
            {
                PolyLine_shape plsh = (PolyLine_shape)sh.polylines[0];
                Point[] plist = new Point[plsh.Points.Count];
                for (int j = 0; j < plsh.Points.Count; j++)
                {
                    Point_shape ps = (Point_shape)plsh.Points[j];
                    double sx = 0, sy = 0;
                    imgcd.ToScreen( ps.X, Math.Abs(ps.Y), ref sx, ref sy);
                    plist[j] = new Point((int)sx, (int)sy);
                }
                g.DrawLines(shpPens, plist);
            }
            g.Dispose();
            return bitm;
        }
        Image FileToBitmap(string fileName)
        {
            // 打开文件    
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            // 读取文件的 byte[]    
            byte[] bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, bytes.Length);
            fileStream.Close();
            // 把 byte[] 转换成 Stream    
            MemoryStream stream = new MemoryStream(bytes);

            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始    
            stream.Seek(0, SeekOrigin.Begin);


            try
            {
                //MemoryStream mstream = new MemoryStream(bytes);
                return Image.FromStream(stream);
                //  new Bitmap(stream);
            }
            catch (ArgumentNullException ex)
            {
                return null;
            }
            catch (ArgumentException ex)
            {
                return null;
            }
            finally
            {
                stream.Close();
            }
        }

        int level = 0;
        terrain[,] terraindome = new terrain[0, 0];
        int countt = 0;
       public   Coordinate dencoor = new Coordinate();
        public double[,] DEM2;
        public latlon GetlatlonByXY(float x, float y)
        {
            latlon lo = new latlon();
            double px = x, py = y, sx=0,sy=0;
            //dencoorimng.ToCoordinate(  x, y, ref px, ref py);
            lo.lat = (float)x;
            lo.lon = (float)y;
            dencoor.ToScreen(px, py, ref sx, ref sy);
            try { lo.high = (int)DEM2[(int)sx, (int)sy]; } catch { }
            return lo;
        }
        Mesh mesh2;
        public void VertexDeclaration(D3dDraw _device)//定义顶点
        {

            String heightMapPath = @fildem;//定义高度图路径 
            String imgpath = @fileimg;
            //terrainlevelzreo.texture = TextureLoader.FromFile(_device.Device, imgpath);
            DEMode dm = new DEMHepler().GetDEM(heightMapPath);

         
             DEM2 = dm.DEM;
            int row2 = DEM2.GetLength(0);
            int col2 = DEM2.GetLength(1);
            dencoor.SetCoordinate(row2, col2, dm.minlng, dm.Maxlng, dm.minlat, dm.Maxlat);
            string bitmapPath = heightMapPath;

            Bitmap bitm = new Bitmap(imgpath);
            byte[,] DEM = new DEMHepler().GetDEMbyimg(bitm);
            
            DEMode dmimg = new DEMHepler().GetDEMbypjw(fileimg, DEM.GetLength(0), DEM.GetLength(1));
            int rows = dmimg.imgWidth; int cols = dmimg.imgHeight;
            dencoorimng.SetCoordinate(rows, cols, dmimg.minlng, dmimg.Maxlng, dmimg.minlat, dmimg.Maxlat);
          // byte[,] bianyuan = new DEMHepler().edge(DEM, rows / 2, cols / 2, dencoorimng);
            //terrainlevelzreo.DEM = DEM;
            int lw = 0, lh = 0;
            draw(_device, terrainlevelzreo, rows, cols, DEM, DEM2, dencoor, dencoorimng, ref lw, ref lh);
            // _device.Device.Material = material;

            if (shpstr != "")
            {
                bitm= Drawshp(bitm, dencoorimng);
            }
            //bitm.Save("test.png", System.Drawing.Imaging.ImageFormat.Jpeg);
            MemoryStream ms = new MemoryStream();
            bitm.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            ms.Position = 0;
            bitm.Dispose();
            bitm = null;
            terrainlevelzreo.texture = TextureLoader.FromStream(_device.Device, ms);
            // terrainlevelzreo.texture = TextureLoader.FromStream(_device.Device, ms, DEM.GetLength(0), DEM.GetLength(1),0,Usage.Dynamic,);
            ms.Dispose();
            ms = null;
            GC.Collect();


            int maxheight = 0;
            for (int i = 0; i < row2; i++)
            {
                for (int j = 0; j < col2; j++)
                {

                    int height =(int) DEM2[i, j];
                    if (height > maxheight)
                    {
                        maxheight = height;
                    }
                }
            }
           // bianyuanVertex= drawbianyuan(_device, bianyuan, DEM2, dencoor, dencoorimng);
            Mesh mesh = new Mesh(terrainlevelzreo.indices.Length / 3, terrainlevelzreo.vertices.Length, MeshFlags.Managed, CustomVertex.PositionColoredTextured.Format, _device.Device);
            //顶点缓冲
            GraphicsStream vs = mesh.LockVertexBuffer(LockFlags.None);
            vs.Write(terrainlevelzreo.vertices);
            mesh.UnlockVertexBuffer();
            vs.Dispose();

            //索引缓冲
            GraphicsStream ids = mesh.LockIndexBuffer(LockFlags.None);
            ids.Write(terrainlevelzreo.indices);
            mesh.UnlockIndexBuffer();
            ids.Dispose();
            if (Surrounded)
            {
                mesh2 = new Mesh(terrainlevelzreo.indices.Length / 3, terrainlevelzreo.vertices2.Length, MeshFlags.Managed, CustomVertex.PositionColored.Format, _device.Device);
                //顶点缓冲
                GraphicsStream vs2 = mesh2.LockVertexBuffer(LockFlags.None);
                vs2.Write(terrainlevelzreo.vertices2);
                mesh2.UnlockVertexBuffer();
                vs2.Dispose();

                //索引缓冲
                GraphicsStream ids2 = mesh2.LockIndexBuffer(LockFlags.None);
                ids2.Write(terrainlevelzreo.indices);
                mesh2.UnlockIndexBuffer();
                ids2.Dispose();
            }
            terrainlevelzreo.Meshobj = mesh;

            System.Threading.Thread.Sleep(100);
            //_device.Camera.setCamera((float)cols * 4);
            lock (this)
            {
                //_device.Camera.CamPostion = new Vector3(rows * 2, cols * 2, (float)((maxheight + 1500)));
               
                float height = (float)DEM2[row2 / 2, col2 / 2];
                double px = 0, py = 0;
              
                dencoorimng.ToCoordinate(rows / 2, cols / 2, ref px, ref py);
              
                _device.Camera.CamPostion = new Vector3((float)dm.minlng, (float)dm.minlat, (float)(basehigh+2.2 + (maxheight  * hyperbole)));
                _device.Camera.CamTarget = new Vector3((float)px, (float)py, (float)basehigh+(height * hyperbole));
                //  _device.Camera.CamTarget = new Vector3(rows / 2, cols / 2, (float)height);
                _device.Camera.SetUpCamera();
                _device.Camera.Setdefault(_device.Camera.CamPostion, _device.Camera.CamTarget);
            }
        }
        Mesh bianyuanVertex;

        void M4_4(D3dDraw _device)
            {
            string heightMapPath = @fildem;//定义高度图路径 
            String imgpath = @fileimg;
            DEMode dm = new DEMHepler().GetDEM(heightMapPath);

            Coordinate dencoor = new Coordinate();
            double[,] DEM2 = dm.DEM;
            int row2 = DEM2.GetLength(0);
            int col2 = DEM2.GetLength(1);
            dencoor.SetCoordinate(row2, col2, dm.minlng, dm.Maxlng, dm.minlat, dm.Maxlat);
            string bitmapPath = heightMapPath;
            int rows = 0; int cols = 0;
            int len = 2;
            terraindome = new terrain[len, len];
            for (int i = 0; i < len; i++)
            {
                for (int j = 0; j < len; j++)
                {
                    string img = (@"D:\testmap\" + ((i * len) + j) + ".png");

                    img ii = new img();
                    ii.i = i;
                    ii.j = j;
                    ii.imgstr = img;

                    jiazai(ii);
                }
            }

            for (int i = 0; i < len; i++)
            {
                for (int j = 0; j < len; j++)
                {




                    terraindome[i, j].W = terraindome[i, j].DEM.GetLength(0);
                    terraindome[i, j].H = terraindome[i, j].DEM.GetLength(1);
                    //if(terraindome[i, j - 1])


                    if (j == 0)
                        terraindome[i, j].X = 0;
                    else
                    {

                        terraindome[i, j].X = terraindome[i, j - 1].X + terraindome[i, j - 1].W;
                    }
                    if (i == 0)
                        terraindome[i, j].Y = 0;
                    else
                    {
                        terraindome[i, j].Y = terraindome[i - 1, 0].Y + terraindome[i - 1, 0].H;
                    }

                }
            }
            int maxheight = 0;
            try
            {


                rows = terraindome[len - 1, len - 1].X + terraindome[len - 1, len - 1].W;
                cols = terraindome[len - 1, len - 1].Y + terraindome[len - 1, len - 1].H;
                DEMode dmimg = new DEMHepler().GetDEMbypjw(imgpath, rows, cols);
                dencoorimng.SetCoordinate(rows, cols, dmimg.minlng, dmimg.Maxlng, dmimg.minlat, dmimg.Maxlat);

                for (int i = 0; i < row2; i++)
                {
                    for (int j = 0; j < col2; j++)
                    {

                        int height = (int)DEM2[i, j];
                        if (height > maxheight)
                        {
                            maxheight = height;
                        }
                    }
                }
                var device = _device.Device;


            }
            catch
            { }

            try
            {
                int lw = 0, lh = 0;
                for (int i = 0; i < len; i++)
                {
                    for (int j = 0; j < len; j++)
                    {
                        draw(_device, terraindome[i, j], terraindome[i, j].W, terraindome[i, j].H, terraindome[i, j].DEM, DEM2, dencoor, dencoorimng, ref lw, ref lh);

                    }
                }
            }
            catch (Exception e)
            { }
            try
            {
                material = new Material();
                material.Diffuse = Color.Transparent;
                material.Specular = Color.LightGray;
                material.SpecularSharpness = 15.0F;
                _device.Device.Material = material;
                for (int i = 0; i < len; i++)
                {
                    for (int j = 0; j < len; j++)
                    {
                        terraindome[i, j].DEM = new byte[0, 0];
                        terraindome[i, j].DEM2 = new int[0, 0];
                        if (terraindome[i, j].index != 0)
                        {
                            String str = @"D:\testmap\" + ((i * len) + j) + ".png";
                            terraindome[i, j].texture = TextureLoader.FromFile(_device.Device, str);
                        }

                    }
                }
            }
            catch (Exception e)
            { }
            

        }
        Coordinate dencoorimng = new Coordinate();
        class img
        {
           public int i;
            public int j;
            public string imgstr;
        }
        void jiazai(object obj)
        {
            img iim = obj as img;
            //byte[,] DEM = new DEMHepler().GetDEMbyimg(iim.imgstr);
            //int i = iim.i;
            //int j = iim.j;
            //terraindome[i, j] = new terrain();
            //terraindome[i, j].DEM = DEM;
            countt++;
        }

        Mesh drawbianyuan(D3dDraw _device, List<double[]> bianyuan, int[,] DEM2, Coordinate dencoor, Coordinate dencoorimng)
        {
            var device = _device.Device;
            int index = bianyuan.Count;
            VertexBuffer vertexBuffer2 = new
     VertexBuffer(typeof(CustomVertex.PositionColored), index,
     device,
      Usage.Dynamic | Usage.WriteOnly,
     CustomVertex.PositionColored.Format, Pool.Default);
            CustomVertex.PositionColored[] vertices2 = new CustomVertex.PositionColored[index];
            int j = 0;
            try
            {
                int height = 0;
                for (int i = 0; i < index ; i = i + 2)
                {
                    double[] xy = bianyuan[i];
                    double px = 0, py = 0, sx = 0, sy = 0;
                    int xx = (int)xy[0];
                    int yy = (int)xy[1];
                    dencoorimng.ToCoordinate(xx, yy, ref px, ref py);
                    dencoor.ToScreen(px, py, ref sx, ref sy);

                    int heighttmp = DEM2[(int)sx, (int)sy];
                    if (heighttmp != 0)
                        height = heighttmp;
                    vertices2[j].Position = new Vector3((int)xy[0], (int)xy[1], 0);
                    vertices2[j].Color = Color.Black.ToArgb();
                    j++;
                    vertices2[j].Position = new Vector3((int)xy[0], (int)xy[1], height);
                    vertices2[j].Color = Color.Black.ToArgb();
                    j++;
                }
            }
            catch(Exception e)
            { }
            vertexBuffer2.SetData(vertices2, 0, LockFlags.None);
            
          //  index = 6;
            IndexBuffer indexBuffer = new IndexBuffer(typeof(int), 6 * index, device, Usage.WriteOnly, Pool.Default);
            short[] indices2 = new short[6 * index];
            int s = 0;
            for (int i = 0; i < indices2.Length; i=i+6)
            {
                
                indices2[i] = (short)s;
                indices2[i + 1] = (short)(s + 1);
                indices2[i + 2] = (short)(s + 2);
                indices2[i + 3] = (short)(s + 2);
                indices2[i + 4] = (short)(s + 1);
                indices2[i + 5] = (short)(s + 3);
                
                s += 2;
            }
            indexBuffer.SetData(indices2, 0, LockFlags.None);
            Mesh meshmm = new Mesh(indices2.Length / 3, vertices2.Length, MeshFlags.Managed, CustomVertex.PositionColored.Format, _device.Device);
            //顶点缓冲
            GraphicsStream vs = meshmm.LockVertexBuffer(LockFlags.None);
            vs.Write(vertices2);
            meshmm.UnlockVertexBuffer();
            vs.Dispose();

            //索引缓冲
            GraphicsStream ids = meshmm.LockIndexBuffer(LockFlags.None);
            ids.Write(indices2);
            meshmm.UnlockIndexBuffer();
            ids.Dispose();
            return meshmm;
        }
        void draw(D3dDraw _device,terrain tra,int row,int col, byte[,] DEM, double[,] DEM2,Coordinate dencoor , Coordinate dencoorimng,ref int lastw,ref int lasth)
        {
          
            tra.xCount = (row - 1) / multiple;
            tra.yCount = (col - 1) / multiple;
            tra.cellWidth = row / tra.xCount;
            tra.cellHeight = col / tra.yCount;

            //  Bitmap bit = new Bitmap(DEM2.GetLength(0), DEM2.GetLength(1));
            //int count = 0;
            //for (int I = 0; I < DEM2.GetLength(1); I++)
            //{
            //    for (int j = 0; j < DEM2.GetLength(0); j++)
            //    {
                 
            //        if (DEM2[j, I] != 0)
            //            bit.SetPixel(j, I, Color.White);
            //    }
            //}
            // bit.Save("ttt.png");
            var device = _device.Device;

            tra.vertexBuffer = new
  VertexBuffer(typeof(CustomVertex.PositionColoredTextured), (tra.xCount + 1) * (tra.yCount + 1),
  device,
   Usage.Dynamic | Usage.WriteOnly,
  CustomVertex.PositionColored.Format, Pool.Default);
            tra.vertices = new CustomVertex.PositionColoredTextured[(tra.xCount + 1) * (tra.yCount +
         1)];
            if (Surrounded)
            {
                tra.vertexBuffer2 = new
  VertexBuffer(typeof(CustomVertex.PositionColored), (tra.xCount + 1) * (tra.yCount + 1),
  device,
   Usage.Dynamic | Usage.WriteOnly,
  CustomVertex.PositionColored.Format, Pool.Default);

                tra.vertices2 = new CustomVertex.PositionColored[(tra.xCount + 1) * (tra.yCount +
               1)];
            }
            tra.index = 0;
            int maxheight = 0;
            tra.indices = new short[6 * tra.xCount * tra.yCount];
            byte[,] bianyuan2 = new byte[tra.xCount+1, tra.yCount+1];
         //   Bitmap bit = new Bitmap(tra.xCount, tra.yCount);
            for (int i = 0; i < tra.yCount; i++)
            {
                for (int j = 0; j < tra.xCount; j++)
                {
                    //double px = 0, py = 0, sx = 0, sy = 0;
                    //int xx = (int)((tra.X) + j * tra.cellWidth);
                    //int yy = (int)((tra.Y) + i * tra.cellHeight);
                    //dencoorimng.ToCoordinate(xx, yy, ref px, ref py);

                      int id = DEM[(int)(j * tra.cellWidth), (int)(i * tra.cellHeight)];
                    //int xx = (int)((tra.X) + j * tra.cellWidth);
                    //int yy = (int)((tra.Y) + i * tra.cellHeight);
                    //double px = 0, py = 0, sx = 0, sy = 0;
                    //dencoorimng.ToCoordinate(xx, yy, ref px, ref py);
                    //dencoor.ToScreen(px, py, ref sx, ref sy);
                    //bianyuan2[j, i] = (byte)id;
                    //if (sx > DEM2.GetLength(0) || sy > DEM2.GetLength(1))
                    //{

                    //}
                    //double height = DEM2[(int)sx, (int)sy];
                    //if(height>0)
                    //bit.SetPixel(j, i, Color.White);
                    //dencoor.ToScreen(px, py, ref sx, ref sy);

                    //int height = DEM2[(int)sx, (int)sy];

                    if (id > 0)
                        bianyuan2[j, i] = (byte)1;


                }

            }
          //  bit.Save("ttt22.png");
            byte[,] bianyuan = new DEMHepler().edge(bianyuan2, row / 2, col / 2, dencoorimng);
 
            for (int i = 0; i < tra.yCount + 1; i++)
            {
                for (int j = 0; j < tra.xCount + 1; j++)
                {
                    try
                    {
                        double px = 0, py = 0, sx = 0, sy = 0;
                        int xx = (int)((tra.X) + j * tra.cellWidth);
                        int yy = (int)((tra.Y) + i * tra.cellHeight);
                        dencoorimng.ToCoordinate(xx, yy, ref px, ref py);
                    
                        int id = DEM[(int)(j * tra.cellWidth), (int)(i * tra.cellHeight)];
                        dencoor.ToScreen(px, py, ref sx, ref sy);

                        double height = DEM2[(int)sx, (int)sy];
                        if(tra.X!=0)
                         xx = (int)((tra.X - lastw) + j * tra.cellWidth);
                        if(tra.Y!=0)
                         yy = (int)((tra.Y - lasth) + i * tra.cellHeight);

                       
                        float hh = (float)decimal.Round((decimal)(basehigh + ((float)height * hyperbole)), 2);
                         
                          
                        tra.vertices[j + i * (tra.xCount + 1)].Position = new Vector3((float)px, (float)py, hh); 
                        tra.vertices[j + i * (tra.xCount + 1)].Color = Color.FromArgb(0, 255, 255, 255).ToArgb(); 
                        tra.vertices[j + i * (tra.xCount + 1)].Tu = (float)j / (tra.xCount + 1);
                        tra.vertices[j + i * (tra.xCount + 1)].Tv = (float)i / (tra.yCount + 1);
                       // if (id != 0)
                        //if (height != 0)
                        {
                             
                            if (height > maxheight)
                            {
                                maxheight =(int) height;
                            }
                            if (height !=0)
                            {
                               
                                tra.vertices[j + i * (tra.xCount + 1)].Color = Color.FromArgb((int)(255 * transparency), 255, 255, 255).ToArgb();
                                tra.index++;
                             
                            }
                             
                        }
                        
                        if (Surrounded)
                        {
                            if (bianyuan[j, i] == 1)
                            {
                                tra.vertices2[j + i * (tra.xCount + 1)].Position = new Vector3((float)px, (float)py, hh);
                                
                                     tra.vertices2[j + i * (tra.xCount + 1)].Color = Color.FromArgb(250, 235, 215).ToArgb();
                            }
                            else {
                                tra.vertices2[j + i * (tra.xCount + 1)].Position = new Vector3((float)px, (float)py, basehigh + 0f);

                                tra.vertices2[j + i * (tra.xCount + 1)].Color = Color.FromArgb(250, 235, 215).ToArgb();
                            }
                        }
                        // tra.index++;
                    }
                    catch
                    { }
                  //  tra.index++;

                    // }
                }
            }
          
            lastw = tra.W-( tra.xCount * (int)tra.cellWidth);
            lasth = tra.H-( tra.yCount * (int)tra.cellHeight);
            tra.vertexBuffer.SetData(tra.vertices, 0, LockFlags.None);
            if (Surrounded)
            {
                tra.vertexBuffer2.SetData(tra.vertices2, 0, LockFlags.None);
            }
            IndicesDeclaration(device,tra, DEM, dencoorimng, DEM2, dencoor, tra.index);
        }

        bool getv3heigth(CustomVertex.PositionColoredTextured[] vertices, int index)
        {
            Vector3 v3 = vertices[index].Position;
            if (v3.Z == 0)
                return false;
            return true;
        }
        public void IndicesDeclaration(Microsoft.DirectX.Direct3D.Device device, terrain tra, byte[,] DEM, Coordinate dencoorimng, double[,] DEM2, Coordinate dencoor, int index)//定义索引
        {

            int j = 0, s = 0;
            index = 0;
            for (int i = 0; i < tra.yCount; i++)
            {
                for (j = 0; j < tra.xCount; j++)
                {
                    tra.indices[6 * (j + i * tra.xCount)] = -1;
                    int xx = (int)((tra.X ) + j * tra.cellWidth);
                    int yy = (int)((tra.Y ) + i * tra.cellHeight);
                    if (DEM[(int)(j * tra.cellWidth), (int)(i * tra.cellHeight)] > 0)
                    {
                        try
                        {
                            double px = 0, py = 0, sx = 0, sy = 0;
                            dencoorimng.ToCoordinate(xx, yy, ref px, ref py);
                            dencoor.ToScreen(px, py, ref sx, ref sy);
                            double height = DEM2[(int)sx, (int)sy];

                            if (height > 0)
                            {
                                if (getv3heigth(tra.vertices, j + i * (tra.xCount + 1)) && getv3heigth(tra.vertices, j + (i + 1) * (tra.xCount + 1))
                                      && getv3heigth(tra.vertices, j + (i + 1) * (tra.xCount + 1)) && getv3heigth(tra.vertices, j + (i + 1) * (tra.xCount + 1) + 1)
                                      && getv3heigth(tra.vertices, j + i * (tra.xCount + 1) + 1)
                                      )
                                {
                                    tra.indices[6 * (j + i * tra.xCount)] = (short)(j + i * (tra.xCount + 1));
                                    tra.indices[6 * (j + i * tra.xCount) + 1] = (short)(j + (i + 1) * (tra.xCount + 1));
                                    tra.indices[6 * (j + i * tra.xCount) + 2] = (short)(j + i * (tra.xCount + 1) + 1);
                                    tra.indices[6 * (j + i * tra.xCount) + 3] = (short)(j + i * (tra.xCount + 1) + 1);
                                    tra.indices[6 * (j + i * tra.xCount) + 4] = (short)(j + (i + 1) * (tra.xCount + 1));
                                    tra.indices[6 * (j + i * tra.xCount) + 5] = (short)(j + (i + 1) * (tra.xCount + 1) + 1);
                                    s = s + 6;
                                    index++;
                                }
                            }
                        }
                        catch
                        { }
                    }

                }
            }
            if (index == 0)
            {
                tra.index = 0;
                tra.vertexBuffer = null;
                tra.vertexBuffer2 = null;
                tra.vertices = null;
                tra.vertices2 = null;
                tra.indices = null;
                tra.DEM = null;
                return;
            }
            // indexBuffer = new IndexBuffer(typeof(int), 6 * xCount * yCount, device, Usage.WriteOnly, Pool.Default);
            try
            {
                tra.indexBuffer = new IndexBuffer(typeof(int), 6 * index, device, Usage.WriteOnly, Pool.Default);
                short[] indices2 = new short[6 * index];
                j = 0;
                for (int i = 0; i < tra.indices.Length; i = i + 6)
                {
                    if (tra.indices[i] != -1)
                    {
                        indices2[j] = tra.indices[i];
                        indices2[j + 1] = tra.indices[i + 1];
                        indices2[j + 2] = tra.indices[i + 2];
                        indices2[j + 3] = tra.indices[i + 3];
                        indices2[j + 4] = tra.indices[i + 4];
                        indices2[j + 5] = tra.indices[i + 5];
                        j += 6;
                    }
                    else
                    { tra.indices[i] = 0; }
                }

                tra.indices = indices2;
                //tra.indexBuffer2 = new IndexBuffer(typeof(int), 6 * index, device, Usage.WriteOnly, Pool.Default);
                //tra.indexBuffer2.SetData(indices2, 0, LockFlags.None);
                tra.indexBuffer.SetData(tra.indices, 0, LockFlags.None);
             
            }
            catch
            { }
        }

        public void RenderDraw(D3dDraw _device)
        {
            
            var device = _device.Device;

            //_device.Device.RenderState.AlphaBlendEnable = true;
            //_device.Device.RenderState.AlphaTestEnable = true;
            //_device.Device.RenderState.ReferenceAlpha = 0;
            //_device.Device.RenderState.AlphaFunction = Compare.Greater;
            //_device.Device.RenderState.SourceBlend = Blend.SourceAlpha;
            //_device.Device.RenderState.DestinationBlend = Blend.BothInvSourceAlpha;
            //_device.Device.RenderState.BlendOperation = BlendOperation.Add;

            device.BeginScene();
            try
            {
                //device.RenderState.Lighting = true;
                //device.RenderState.ZBufferEnable = true;
                ////  D3device.Device.RenderState.ZBufferEnable = false;		 	//允许使用深度缓冲
                //device.RenderState.Ambient = System.Drawing.Color.White;//设定环境光为白色
                //device.Lights[0].Type = LightType.Directional;     //设置灯光类型
                //device.Lights[0].Diffuse = Color.White;            //设置灯光颜色
                //device.Lights[0].Direction = new Vector3(1, -1, -1); //设置灯光位置
                //device.Lights[0].Enabled = true;

                VertexFormats format = device.VertexFormat;
                //获取当前的Z缓冲方式
                //int zEnable = device.GetRenderStateInt32(RenderStates.ZEnable);
                ////获取纹理状态
                //int colorOper = device.GetTextureStageStateInt32(0, TextureStageStates.ColorOperation);

                if (level != 0)
                {
                    for (int i = 0; i < terraindome.GetLength(0); i++)
                    {
                        for (int j = 0; j < terraindome.GetLength(1); j++)
                        {
                            if (terraindome[i, j].index != 0)
                            {
                                try
                                {
                                    if (terraindome[i, j].texture != null)
                                        device.SetTexture(0, terraindome[i, j].texture);//设置贴图
                                    device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
                                    terraindome[i, j].Meshobj.DrawSubset(0);
                                }
                                catch { }
                            }
                        }
                    }
                }

                // device.DrawUserPrimitives(PrimitiveType.TriangleList, 2, terrainlevelzreo.vertices2);
                device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
                if (terrainlevelzreo.texture != null)
                    device.SetTexture(0, terrainlevelzreo.texture);//设置贴图

                //device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                //device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                //device.TextureState[0].ColorArgument2 = TextureArgument.Current;
                if (!Surrounded)
                    device.TextureState[0].AlphaOperation = TextureOperation.BlendCurrentAlpha;

                // device.DrawUserPrimitives(PrimitiveType.TriangleList, 2, terrainlevelzreo.vertices);
                // device.SetStreamSource(0, terrainlevelzreo.vertexBuffer, 0);
                // device.Indices = terrainlevelzreo.indexBuffer;
                // device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                //(terrainlevelzreo.xCount + 1) * (terrainlevelzreo.yCount + 1), 0, terrainlevelzreo.indices.Length / 3);
                terrainlevelzreo.Meshobj.DrawSubset(0);

                //device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                //device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                //device.TextureState[0].ColorArgument2 = TextureArgument.Diffuse;
                device.TextureState[0].AlphaOperation = TextureOperation.Disable;
                if (Surrounded)
                    mesh2.DrawSubset(0);
                // bianyuanVertex.DrawSubset(0);
                // device.SetStreamSource(0, terrainlevelzreo.vertexBuffer, 0);
                // device.Indices = terrainlevelzreo.indexBuffer;
                // device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                //(terrainlevelzreo.xCount + 1) * (terrainlevelzreo.yCount + 1), 0, terrainlevelzreo.indices.Length / 3);
                //device.Lights[0].Update();
                //device.RenderState.Lighting = false;
                device.VertexFormat = format;
                //device.SetRenderState(RenderStates.ZEnable, zEnable);
                //device.SetTextureStageState(0, TextureStageStates.ColorOperation, colorOper);
                device.VertexFormat = CustomVertex.PositionColored.Format;
            }
            catch { }
            device.EndScene();
          
                                                  
        }

        public void Initialize(D3dDraw _device)
        {
            LoadTexturesAndMaterials(_device.Device);
            VertexDeclaration(_device);

            isinit = true;
        }

        public bool GetIsinit()
        {
            return isinit;
        }

        public Mesh GetMeshobj()
        {
            return terrainlevelzreo.Meshobj;
        }

        public object GetIDobj()
        {
            return ID;
        }

         

        public Matrix worldMatrix(D3dDraw D3device)
        {
            return D3device.Device.Transform.World;
        }
    }

    public class terrain
    {
        public Mesh Meshobj;
        public Texture texture;//定义贴图变量
        public CustomVertex.PositionColoredTextured[] vertices;
        public CustomVertex.PositionColored[] vertices2;
        public VertexBuffer vertexBuffer;//定义顶点缓冲变量
        public VertexBuffer vertexBuffer2;//定义顶点缓冲变量
        public IndexBuffer indexBuffer;//定义索引缓冲变量
        public IndexBuffer indexBuffer2;//定义索引缓冲变量
        public short[] indices;//定义索引号变量
        public int xCount = 5, yCount = 4;//定义横向和纵向网格数目
        public float cellHeight = 1f, cellWidth = 1.5f;//定义单元的宽度和长度
        public int index = 0;
        public byte[,] DEM;
        public int[,] DEM2;
        public int W = 0;
        public int H = 0;
        public int X = 0;
        public int Y = 0;
    }

    public class latlon
    {
        public float lat;
        public float lon;
        public int high;
    }

}
