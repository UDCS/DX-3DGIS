﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System.Drawing;

namespace D3DMAP.Projects
{
    public class plane : ID3DDraw
    {
        float X, Y, Z; float width, height; float length;
        String imgpath;
        public plane(float _X, float _Y, float _Z, float _width,float _height, string _imgpath)
        {
            X = _X; Y = _Y; Z = _Z; width = _width; height = _height; imgpath = _imgpath;
        }
        public object ID;
        public object GetIDobj()
        {
            return ID;
        }
        bool isinit = false;
        public bool GetIsinit()
        {
            return isinit;
        }

        public Mesh GetMeshobj()
        {
            return null;
        }
        public Texture texture;//定义贴图变量
        public CustomVertex.PositionTextured[] vertices;
        public void Initialize(D3dDraw D3device)
        {
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Specular = Color.LightGray;
            material.SpecularSharpness = 15.0F;
            D3device.Device.Material = material;
            texture = TextureLoader.FromFile(D3device.Device, imgpath);
            vertices = new CustomVertex.PositionTextured[6];
            vertices[0].Position = new Vector3(X , Y , Z);
            vertices[0].Tu = 1;
            vertices[0].Tv = 1;
            vertices[1].Position = new Vector3(X, Y+height, Z);
            vertices[1].Tu = 1;
            vertices[1].Tv = 0;
            vertices[2].Position = new Vector3(X+width, Y , Z);
            vertices[2].Tu = 0;
            vertices[2].Tv = 1;
            vertices[3].Position = new Vector3(X+width, Y+height, Z);
            vertices[3].Tu = 0;
            vertices[3].Tv = 0;
            vertices[4].Position = new Vector3(X+width, Y , Z);
            vertices[4].Tu = 0;
            vertices[4].Tv = 1;
            vertices[5].Position = new Vector3(X , Y+height, Z);
            vertices[5].Tu = 1;
            vertices[5].Tv = 0;
            isinit = true;
        }

        public void RenderDraw(D3dDraw D3device)
        {
            D3device.Device.BeginScene();
            try
            {
             

                //float x_coord = 2.0f, y_coord = 3.0f, z_coord = 5.0f;
                 
                D3device.Device.RenderState.AlphaBlendEnable = true;
                D3device.Device.RenderState.AlphaTestEnable = true;
                D3device.Device.RenderState.ReferenceAlpha = 0;
                D3device.Device.RenderState.AlphaFunction = Compare.Greater;
                D3device.Device.RenderState.SourceBlend = Blend.SourceAlpha;
                D3device.Device.RenderState.DestinationBlend = Blend.BothInvSourceAlpha;
                D3device.Device.RenderState.BlendOperation = BlendOperation.Add;

                D3device.Device.SetTexture(0, texture);//设置贴图
                D3device.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                D3device.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                D3device.Device.TextureState[0].ColorArgument2 = TextureArgument.Current;
                D3device.Device.TextureState[0].AlphaOperation = TextureOperation.BlendCurrentAlpha;
                D3device.Device.VertexFormat = CustomVertex.PositionTextured.Format;
                 

                D3device.Device.DrawUserPrimitives(PrimitiveType.TriangleList, 2, vertices);
                 
                //mesh.DrawSubset(0);
               // D3device.Device.EndScene();
            }
            catch
            {
              
            }
            D3device.Device.EndScene();
        }
    }
}
