﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System.Drawing;
using System.IO;

namespace D3DMAP.Projects
{
    public class Billboard : ID3DDraw
    {
        float X, Y, Z; float width, height;
        private String imgpath;
        private Image billIMG;
        public Billboard(float _X, float _Y, float _Z,float _width, float _height,string _imgpath)
        {
            X = _X; Y = _Y; Z = _Z; width = _width; height = _height;
          

            imgpath = _imgpath;
        }

        public object ID { get; set; }

        public Image BillIMG
        {
             
            set
            {
                billIMG = value;
                //init = false;
                billIMG.RotateFlip(RotateFlipType.Rotate180FlipY);
                MemoryStream ms = new MemoryStream();
                billIMG.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                ms.Position = 0;
                billIMG.Dispose();
                billIMG = null;
                try
                {
                    if (D3d != null)
                        texture = TextureLoader.FromStream(D3d.Device, ms);
                }
                catch { }
              //  init = true;
            }
        }

        public string Imgpath
        {
            get
            {
                return imgpath;
            }

            set
            {
                imgpath = value; 
                if(D3d!=null)
                  texture = TextureLoader.FromFile(D3d.Device, imgpath);
                
                
            }
        }

        public object GetIDobj()
        {
            return ID;
        }
        bool init = false;
        public bool GetIsinit()
        {
            return init;
        }
        Mesh mesh = null;
        public Mesh GetMeshobj()
        {
            return mesh;
        }
        D3dDraw D3d = null;
        public Texture texture;//定义贴图变量
        public CustomVertex.PositionTextured[] vertices;
        //public CustomVertex.PositionColored[] verticesmesh;
        //public VertexBuffer vertexBuffer;//定义顶点缓冲变量 
        //public IndexBuffer indexBuffer;//定义索引缓冲变量
        //public short[] indices;//定义索引号变量
        public void Initialize(D3dDraw D3device)
        {
            init = false;
            D3d = D3device;
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Specular = Color.LightGray;
            material.SpecularSharpness = 15.0F;
            D3device.Device.Material = material;
            if (imgpath == "")
            {
                Bitmap bit = new Bitmap(20,20 );
                MemoryStream ms = new MemoryStream();
                bit.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                ms.Position = 0;
                bit.Dispose();
                bit = null;
                texture = TextureLoader.FromStream(D3device.Device, ms);
            }
            else
                texture = TextureLoader.FromFile(D3device.Device, Imgpath);
            vertices = new CustomVertex.PositionTextured[6];
            vertices[0].Position = new Vector3(0-(width/2),0-(height/2), 0);
            vertices[0].Tu = 1;
            vertices[0].Tv = 1;
            vertices[1].Position = new Vector3(0 - (width / 2), height/2, 0 );
            vertices[1].Tu = 1;
            vertices[1].Tv = 0;
            vertices[2].Position = new Vector3(width/2,0 - (height / 2), 0 );
            vertices[2].Tu = 0;
            vertices[2].Tv = 1;
            vertices[3].Position = new Vector3(width/2, height/2, 0);
            vertices[3].Tu = 0;
            vertices[3].Tv = 0;
            vertices[4].Position = new Vector3(width/2, 0 - (height / 2), 0);
            vertices[4].Tu = 0;
            vertices[4].Tv = 1;
            vertices[5].Position = new Vector3(0 - (width / 2), height/2, 0);
            vertices[5].Tu = 1;
            vertices[5].Tv = 0;
            //包围盒
            
            Sphere sp = new Sphere( D3device,new Vector3(X, Y, Z), width / 2, 6, 6);
            mesh = sp.Getmesh();
            init = true;
        }

        public void RenderDraw(D3dDraw D3device)
        {
            D3device.Device.BeginScene();
            try
            {
              

                //float x_coord = 2.0f, y_coord = 3.0f, z_coord = 5.0f;

                Matrix mat_trans = Matrix.Identity, mat_world = Matrix.Identity;
                Matrix mat_transpose = D3device.Device.GetTransform(TransformType.View);
                Matrix World = D3device.Device.GetTransform(TransformType.World);

                Vector3 m_vEye = D3device.Camera.CamPostion; //观察者眼睛的位置
                Vector3 m_vLookat = new Vector3(X, Y, Z); //公告牌位置 
                                                          // Matrix matRotate= D3device.Device.GetTransform(TransformType.World); //旋转矩阵  
                Vector3 m_vUp = new Vector3(0, 0, 1);
                Vector3 m_vView = Vector3.Normalize(m_vLookat - m_vEye);
                Vector3 m_vCross = Vector3.Cross(m_vUp, m_vView);
                Matrix viewMatrix = Matrix.LookAtLH(m_vEye, m_vLookat, m_vUp);
                viewMatrix.Invert();
                // 获取 billboard ( view matrix 的转置矩阵就是 billboard )   

                viewMatrix.M41 = 0.0f;
                viewMatrix.M42 = 0.0f;
                viewMatrix.M43 = 0.0f;

                Matrix newm = Matrix.Translation(m_vLookat);
                Matrix matScal = Matrix.Identity;

                D3device.Device.Transform.World = matScal * viewMatrix * newm;
               
                D3device.Device.RenderState.AlphaBlendEnable = true;
                D3device.Device.RenderState.AlphaTestEnable = true; 
                D3device.Device.RenderState.ReferenceAlpha = 0; 
                D3device.Device.RenderState.AlphaFunction = Compare.Greater; 
                D3device.Device.RenderState.SourceBlend = Blend.SourceAlpha;
                D3device.Device.RenderState.DestinationBlend = Blend.BothInvSourceAlpha;
                D3device.Device.RenderState.BlendOperation = BlendOperation.Add;
             
                D3device.Device.SetTexture(0, texture);//设置贴图
                D3device.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                D3device.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                D3device.Device.TextureState[0].ColorArgument2 = TextureArgument.Current;
                D3device.Device.TextureState[0].AlphaOperation = TextureOperation.BlendCurrentAlpha; 
                D3device.Device.VertexFormat = CustomVertex.PositionTextured.Format;


                

                D3device.Device.DrawUserPrimitives(PrimitiveType.TriangleList, 2, vertices);

                //Mesh meshs = Microsoft.DirectX.Direct3D.Mesh.TextFromFont(D3device.Device, new System.Drawing.Font("宋体", 88), "测试", 1f, 0.5f);
                // matScal = Matrix.Identity;
                // matScal.Scale(100, 100, 100);
                //D3device.Device.Transform.World = matScal * viewMatrix * newm;
                //meshs.DrawSubset(0);
                D3device.Device.SetTransform(TransformType.World, World);
               // D3device.Device.SetRenderState(RenderStates.AlphaBlendEnable, true);
               
                //mesh.DrawSubset(0);
               
            }
            catch
            {
            }
            D3device.Device.EndScene();
        }

       
    }
}
