﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System.Drawing;

namespace D3DMAP.Projects
{
    public class font : ID3DDraw
    {
        public object GetIDobj()
        {
            return null;
        }
        float X, Y, Z; float size; Color fontcolor;
        String imgpath; System.Drawing.Font Fontnmae;
        public font(float _X, float _Y, float _Z, float _size, System.Drawing.Font fontnmae,Color _fontcolor, string Str)
        {
            X = _X; Y = _Y; Z = _Z; size = _size; imgpath = Str; Fontnmae = fontnmae; fontcolor = _fontcolor;
        }
        bool init = false;
        public bool GetIsinit()
        {

            return init;
        }
        Mesh mesh;
        public Mesh GetMeshobj()
        {
            return null;
        }

        public void Initialize(D3dDraw D3device)
        {
            mesh = Microsoft.DirectX.Direct3D.Mesh.TextFromFont(D3device.Device, Fontnmae, imgpath, 1f, 0.5f);
           
            init = true;
        }

        public void RenderDraw(D3dDraw D3device)
        {
            D3device.Device.BeginScene();
            try
            {
               
                
                //float x_coord = 2.0f, y_coord = 3.0f, z_coord = 5.0f;

                Matrix mat_trans = Matrix.Identity, mat_world = Matrix.Identity;
                Matrix mat_transpose = D3device.Device.GetTransform(TransformType.View);
                Matrix World = D3device.Device.GetTransform(TransformType.World);

                Vector3 m_vEye = D3device.Camera.CamPostion; //观察者眼睛的位置
                Vector3 m_vLookat = new Vector3(X, Y, Z); //公告牌位置 
                                                          // Matrix matRotate= D3device.Device.GetTransform(TransformType.World); //旋转矩阵  
                Vector3 m_vUp = new Vector3(0, 0, 1);
                Vector3 m_vView = Vector3.Normalize(m_vLookat - m_vEye);
                Vector3 m_vCross = Vector3.Cross(m_vUp, m_vView);
                Matrix viewMatrix = Matrix.LookAtLH(m_vEye, m_vLookat, m_vUp);
                viewMatrix.Invert();
                // 获取 billboard ( view matrix 的转置矩阵就是 billboard )   

                viewMatrix.M41 = 0.0f;
                viewMatrix.M42 = 0.0f;
                viewMatrix.M43 = 0.0f;

                Matrix newm = Matrix.Translation(m_vLookat);
                Matrix matScal = Matrix.Identity;
                 matScal.Scale(size, size, size);

               
               
             
                D3device.Device.Transform.World = matScal * viewMatrix * newm;
                D3device.Device.RenderState.Lighting = true;
                D3device.Device.RenderState.ZBufferEnable = true;
              //  D3device.Device.RenderState.ZBufferEnable = false;		 	//允许使用深度缓冲
                D3device.Device.RenderState.Ambient = System.Drawing.Color.White;//设定环境光为白色
                D3device.Device.Lights[0].Type = LightType.Directional;  	//设置灯光类型
                D3device.Device.Lights[0].Diffuse = Color.White;			//设置灯光颜色
                D3device.Device.Lights[0].Direction = new Vector3(0, 0, 0); //设置灯光位置
                D3device.Device.Lights[0].Enabled = true;					//使设置有效
                D3device.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                D3device.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                D3device.Device.TextureState[0].ColorArgument2 = TextureArgument.Diffuse;
                D3device.Device.TextureState[0].AlphaOperation = TextureOperation.Disable;
                Material material = new Material();
                material.Diffuse = Color.White;
                material.Specular = Color.Black;
                material. Ambient = fontcolor;
                material.SpecularSharpness = 15.0f;
                D3device.Device.Material = material;
                mesh.DrawSubset(0); 
                // D3device.Device.Lights[0].Direction = new Vector3(0, 0, 0); ;
                D3device.Device.Lights[0].Update();
                D3device.Device.RenderState.Lighting = false;
              
                D3device.Device.SetTransform(TransformType.World, World); 
              
            }
            catch
            { }
            D3device.Device.EndScene();
        }
    }
}