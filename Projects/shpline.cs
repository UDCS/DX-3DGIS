﻿using DX3DGIS;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using static DX3DGIS.shp;

namespace D3DMAP.Projects
{
  public  class shpline : ID3DDraw
    {
        float X, Y, Z; float width, height; float hyperbole;
        String imgpath;
        shp sh = new shp();
        DEMode dm;
        public Coordinate dencoor = new Coordinate();
        public shpline(float size, string _shppath,string hgihimg, float _hyperbole)
        {
            width = size;
            hyperbole = _hyperbole;
               imgpath = _shppath;
            dm = new DEMHepler().GetDEM(hgihimg);
           
            int row2 = dm.DEM.GetLength(0);
            int col2 = dm.DEM.GetLength(1);
            dencoor.SetCoordinate(row2, col2, dm.minlng, dm.Maxlng, dm.minlat, dm.Maxlat);
            FileStream fs = System.IO.File.Open(imgpath, FileMode.Open);
            sh.read(fs);
            fs.Close();
        }
        public object ID;
        public object GetIDobj()
        {
            return ID;
        }
        bool isinit = false;
        public bool GetIsinit()
        {
            return isinit;
        }

        public Mesh GetMeshobj()
        {
            return null;
        }
        public Texture texture;//定义贴图变量
        public CustomVertex.PositionTextured[] vertices;
        List<CustomVertex.PositionColored[]> listcvpc = new List<CustomVertex.PositionColored[]>();
        public void Initialize(D3dDraw D3device)
        {
            //Material material = new Material();
            //material.Diffuse = Color.White;
            //material.Specular = Color.LightGray;
            //material.SpecularSharpness = 15.0F;
            //D3device.Device.Material = material;
            if (sh.polylines.Count < 1)
                return;

            for (int i = 0; i < sh.polylines.Count; i++)
            {
                PolyLine_shape plsh = (PolyLine_shape)sh.polylines[0];
                CustomVertex.PositionColored[] axisX2 = new CustomVertex.PositionColored[plsh.Points.Count*2];
                for (int j = 0; j < plsh.Points.Count; j++)
                {
                    Point_shape ps = (Point_shape)plsh.Points[j];
                    double sx = 0, sy = 0;
                    dencoor.ToScreen(ps.X, ps.Y, ref sx, ref sy);
                    double h =  dm.DEM[(int)sx, (int)sy];
                    //axisX2[j*2].Position = new Vector3((float)ps.X- width/2, (float)ps.Y, h* hyperbole);
                    //axisX2[(j * 2)+1].Position = new Vector3((float)ps.X + width/2, (float)ps.Y, h * hyperbole);
                }
                listcvpc.Add(axisX2);
            }
          

            //    axisX2[1].X = v3[0].X;
            //    axisX2[1].Y = v3[0].Y;
            //    axisX2[1].Z = v3[0].Z;
            isinit = true;
        }
        void DrawLine(Sprite batch, Texture blank, Color color, Vector2 point1, Vector2 point2)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = (point2 - point1).Length();
           // batch.Draw(blank, point1, null, color, angle, Vector2.Empty, new Vector2(length, 1), 0);
        }
        public void RenderDraw(D3dDraw D3device)
        {
            D3device.Device.BeginScene();
            try
            {


                //float x_coord = 2.0f, y_coord = 3.0f, z_coord = 5.0f;

                //D3device.Device.RenderState.AlphaBlendEnable = true;
                //D3device.Device.RenderState.AlphaTestEnable = true;
                //D3device.Device.RenderState.ReferenceAlpha = 0;
                //D3device.Device.RenderState.AlphaFunction = Compare.Greater;
                //D3device.Device.RenderState.SourceBlend = Blend.SourceAlpha;
                //D3device.Device.RenderState.DestinationBlend = Blend.BothInvSourceAlpha;
                //D3device.Device.RenderState.BlendOperation = BlendOperation.Add;

                //D3device.Device.SetTexture(0, texture);//设置贴图
                //D3device.Device.TextureState[0].ColorOperation = TextureOperation.Modulate;
                //D3device.Device.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
                //D3device.Device.TextureState[0].ColorArgument2 = TextureArgument.Current;
                //D3device.Device.TextureState[0].AlphaOperation = TextureOperation.BlendCurrentAlpha;
                D3device.Device.VertexFormat = CustomVertex.PositionTextured.Format;

                foreach(CustomVertex.PositionColored[] cps in listcvpc)
                 D3device.Device.DrawUserPrimitives(PrimitiveType.LineList, 2, cps);

                //mesh.DrawSubset(0);
                // D3device.Device.EndScene();
            }
            catch
            {

            }
            D3device.Device.EndScene();
        }
    }
}
