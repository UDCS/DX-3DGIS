﻿using D3DMAP;
using D3DMAP.Projects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            new DEMHepler().calculate(35.67991, 35.646716,112.617167, 112.660082);
        }
        D3DHelper d3dh;
        LandSurface ls;
        Billboard bb2;
        private void Form1_Load(object sender, EventArgs e)
        {
            d3dh = new D3DHelper(this);
            d3dh.DrawMode = Microsoft.DirectX.Direct3D.FillMode.Solid;
            d3dh.Backgroundcolor = Color.FromArgb(83, 83, 83);
                ls = new LandSurface(@"map\1.png", @"map\1高程.tif", 0.0002f);
            //ls = new LandSurface(@"map\大图.png", @"map\洛阳市(5).tif", 0.0002f);
            ls.ID = "12312313";
            ls.Surrounded = false;
            ls.shpstr = "linepath/datongline.shp";
            ls.Surrounded = true;
            d3dh.layers.Add(ls);
            Billboard bb = new Billboard(113.5787f, 39.90427f, 0.3866f, 0.05f, 0.05f, "小雪转中雪.png");
            bb.ID = "小雪转中雪";
            d3dh.layers.Add(bb);

            bb2 = new Billboard(113.456f, 40.05f, 0.3866f, 0.05f, 0.05f, "");
            bb2.ID = "测试";
            d3dh.layers.Add(bb2);
            //Billboard bb2 = new Billboard(113.7887f, 39.70427f, 0.3866f, 0.05f, 0.05f, "干旱.png");
            //bb2.ID = "干旱";
            //d3dh.layers.Add(bb2);

            //plane pp = new plane(112.5787f, 38.90427f, 0.866f, 5f, 5f, "20160605.235834.01.19.png");
            //pp.ID = "雷达";
            //d3dh.layers.Add(pp);


            Xmode xx = new Xmode(113.456f, 40.05f, 0.22f, 0.000003f, "000.x");
            xx.ID = "模型";
            d3dh.layers.Add(xx);

            font ft = new font(113.5887f, 39.90427f, 0.8366f, 0.1f, new System.Drawing.Font("仿宋", 14), Color.Black, "山西大同");
            d3dh.layers.Add(ft);
            // shpline sl = new shpline(1,"linepath/datongline.shp", @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\未命名(4)_高程.tif", 0.0002f);
            //ls = new LandSurface(@"D:\3DGIS\GITD3DMAP\bin\Debug\20180301171328sign.png", @"D:\SGDownload\L13\未命名(4)_高程_大图\L13\未命名(4)_高程.tif", 0.0001f);

            //ls.ID = "12312313";
            //ls.basehigh = 0.1f;
            //ls.transparency = 0.5f;
            //ls.Surrounded = false;
            //ls.multiple = 8;
            //d3dh.layers.Add(ls);
           timer1.Start();
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           

            intersection inter = d3dh.PerformSelectionAction(e.X, e.Y);
            if (inter != null)
            {
                latlon laln = ls.GetlatlonByXY(inter.X, inter.Y);
                if (inter.ID != null)
                    MessageBox.Show("你选中了：" + inter.ID.ToString());
                MessageBox.Show(e.X + "---" + e.Y + "****" + inter.X + "---" + inter.Y + "---" + inter.Z + "*****----" + laln.lat + "---" + laln.lon + "---" + laln.high);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            d3dh.Resetdefault();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Bitmap bit = new Bitmap(80, 60);
            Graphics g= Graphics.FromImage(bit);
            g.DrawString(DateTime.Now.Second + "", new Font("宋体", 34), Brushes.Red, 1, 2);
          
            g.Dispose();
          
            bb2.BillIMG = bit;
        }
    }
}
